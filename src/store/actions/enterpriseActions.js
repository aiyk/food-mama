import axios from 'axios';

import {
    GET_ENTERPRISE,
    GET_ENTERPRISES,
    ENTERPRISE_LOADING,
    GET_ERRORS,
    GET_ALERTS
} from './types';

// Get all enterprises
export const getEnterprises = () => dispatch => {
    dispatch(setEnterpriseLoading());
    axios
        .get('/enterprise')
        .then(res =>
            dispatch({
                type: GET_ENTERPRISES,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ENTERPRISES,
                payload: null
            })
        );
};

//Create enterprise
export const createEnterprise = (enterpriseData, history) => dispatch => {
    console.log(enterpriseData);
    axios
        .post('/enterprise', enterpriseData)
        .then(res => {
            console.log(res.data);
            dispatch(getEnterprises())
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Successfully created enterprise!!!'
                }
            });
        })
        .catch(err => {
            console.log(JSON.stringify(err));
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating enterprise!!!'
                }
            });
        });
}

//Edit enterprise
export const editEnterprise = (enterpriseData, history, enterpriseid) => dispatch => {
    axios
        .patch(`/enterprise/${enterpriseid}`, enterpriseData)
        .then(res => {
            dispatch(setEnterpriseLoading());
            dispatch(getEnterprises());
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Sucessfully edited enterprise!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing enterprise!!!'
                }
            });
        });
}

// Get enterprise by id
export const getEnterpriseById = enterpriseid => dispatch => {
    dispatch(setEnterpriseLoading());

    axios
        .get(`/api/enterprise/${enterpriseid}`)
        .then(res =>
            dispatch({
                type: GET_ENTERPRISE,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ENTERPRISE,
                payload: null
            })
        );
};


// Delete enterprise
export const deleteEnterprise = () => dispatch => {
    if (window.confirm('Are you sure? This can NOT be undone!')) {
        axios
            .delete('/api/enterprise')
            .then(res => {
                dispatch(getEnterprises())
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Successfully deleted enterprise!!!'
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 0,
                        msg: 'Error deleting enterprise!!!'
                    }
                });
            });
    }
};

// Enterprise loading
export const setEnterpriseLoading = () => {
    return {
        type: ENTERPRISE_LOADING
    };
};
