import axios from "axios";
import store from '../Store';

import {
  GET_FOOD,
  GET_FOODS,
  GET_BRANCH_FOODS,
  GET_FOODCATEGORIES,
  FOOD_LOADING,
  GET_ERRORS,
  GET_ALERTS
} from "./types";

let getBranchId = null;

// Get all food
export const getFoods = () => dispatch => {
  dispatch(setFoodLoading());
  axios
    .get("/food?$limit=null&$sort[createdAt]=-1")
    .then(res =>
      dispatch({
        type: GET_FOODS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_FOODS,
        payload: null
      })
    );
};

//Create FOOD
export const createFood = (foodData, history) => dispatch => {
  const config = {
    headers: {
      'content-type': 'multipart/form-data'
    }
  }
  axios
    .post("/food", foodData, config)
    .then(res => {
      dispatch(getFoods());
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 1,
          msg: 'Food was successfully created!!!'
        }
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response
      })
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 0,
          msg: 'Error creating food!!!'
        }
      });
    });
};

//Edit food
export const editFood = (foodData, foodid) => dispatch => {
  const config = {
    headers: {
      'content-type': 'multipart/form-data'
    }
  }
  axios
    .patch(`/food/${foodid}`, foodData, config)
    .then(res => {
      console.log(res);
      dispatch(getFoods());
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 1,
          msg: 'Food successfully edited!!!'
        }
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 0,
          msg: 'Error editing food!!!'
        }
      });
    });
}

//Add to branch foods
export const addFoodToBranch = (foodData, history) => dispatch => {
  if (store.getState().auth.branch.data) {
    if (store.getState().auth.branch.data.length > 0) {
      getBranchId = store.getState().auth.branch.data[0]._id;
    }
  }
  if (getBranchId) {

    foodData.branchId = getBranchId;
    axios
      .post('/branch-food-availability', foodData)
      .then(res => {
        dispatch(
          getBranchFoods()
        )
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 1,
            msg: 'Food successfully added to branch inventory!!!'
          }
        });
        //history.push('/dashboard')
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err
        })
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 0,
            msg: 'Error adding food to branch inventory!!!'
          }
        });
      });
  } else {
    dispatch({
      type: GET_ERRORS,
      payload: 'an error has occured, contact admin'
    })
    dispatch({
      type: GET_ALERTS,
      payload: {
        type: 0,
        msg: 'Error adding food to branch inventory!!!'
      }
    });
  }
}

//remove from branch foods
export const removeFoodFromBranch = (foodid, history) => dispatch => {
  if (store.getState().auth.branch.data) {
    if (store.getState().auth.branch.data.length > 0) {
      getBranchId = store.getState().auth.branch.data[0]._id;
    }
  }
  if (getBranchId) {
    axios
      .delete(`branch-food-availability?branchId=${getBranchId}&foodId=${foodid}`)
      .then(res => {
        dispatch(
          getBranchFoods()
        )
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 1,
            msg: 'Food successfully removed from branch inventory!!!'
          }
        });
        //history.push('/dashboard');
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err
        })
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 1,
            msg: 'Error removing food from branch inventory!!!'
          }
        });
      });
  } else {
    dispatch({
      type: GET_ERRORS,
      payload: 'an error has occured, contact admin'
    })
    dispatch({
      type: GET_ALERTS,
      payload: {
        type: 0,
        msg: 'Error removing food from branch inventory'
      }
    });
  }
}

// Get food by id
export const getFoodById = foodid => dispatch => {
  dispatch(setFoodLoading());

  axios
    .get(`/food/${foodid}`)
    .then(res =>
      dispatch({
        type: GET_FOOD,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_FOOD,
        payload: null
      })
    );
};

// Get branch foods
export const getBranchFoods = () => dispatch => {
  dispatch(setFoodLoading());

  if (store.getState().auth.branch.data) {
    if (store.getState().auth.branch.data.length > 0) {
      getBranchId = store.getState().auth.branch.data[0]._id;
    }
  }
  if (getBranchId) {
    axios
      .get(`/branch-food-availability?branchId=${getBranchId}&$limit=null&$sort[createdAt]=-1`)
      .then(res =>
        dispatch({
          type: GET_BRANCH_FOODS,
          payload: res.data
        })
      )
      .catch(err =>
        dispatch({
          type: GET_BRANCH_FOODS,
          payload: null
        })
      );
  } else {
    dispatch({
      type: GET_BRANCH_FOODS,
      payload: null
    })
  }
};

// Get all food categories
export const getFoodCategories = () => dispatch => {
  //dispatch(setFoodLoading());
  axios
    .get("/food-category")
    .then(res =>
      dispatch({
        type: GET_FOODCATEGORIES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_FOODCATEGORIES,
        payload: null
      })
    );
};

// Delete food
export const deleteFood = (_id) => dispatch => {
  if (window.confirm("Are you sure? This can NOT be undone!")) {
    axios
      .delete(`/food/${_id}`)
      .then(res => {
        dispatch(getFoods())
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 1,
            msg: 'Food successfully deleted!!!'
          }
        });
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 1,
            msg: 'Error deleting food!!!'
          }
        });
      });
  }
};

// food loading
export const setFoodLoading = () => {
  return {
    type: FOOD_LOADING
  };
};
