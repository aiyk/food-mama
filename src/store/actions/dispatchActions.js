import axios from 'axios';
import store from '../Store';

import {
    GET_DISPATCH,
    GET_DISPATCHS,
    DISPATCH_LOADING,
    GET_AVAILABLE_RIDERS,
    GET_ERRORS,
    GET_ALERTS
} from './types';

let getBranchId = null;

//Create Dispatch
export const createDispatch = (dispatchData, history) => dispatch => {
    axios
        .post('/package-manager', dispatchData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Order successfully assigned to rider!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error assigning order to rider!!!'
                }
            });
        });
}

// Get Dispatch by id
export const getDispatchById = dispatchid => dispatch => {
    dispatch(setDispatchLoading());

    axios
        .get(`/api/dispatch/${dispatchid}`)
        .then(res =>
            dispatch({
                type: GET_DISPATCH,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_DISPATCH,
                payload: null
            })
        );
};

// Get all dispatch
export const getDispatchs = () => dispatch => {
    dispatch(setDispatchLoading());
    if (store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
    }
    if (getBranchId) {
        axios
            .get('/api/dispatch/all')
            .then(res =>
                dispatch({
                    type: GET_DISPATCHS,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_DISPATCHS,
                    payload: null
                })
            );
    }
};

// dispatch loading
export const setDispatchLoading = () => {
    return {
        type: DISPATCH_LOADING
    };
};
