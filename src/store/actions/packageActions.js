import axios from 'axios';
import store from '../Store';

import {
    GET_PACKAGE,
    GET_PACKAGES,
    GET_UNASSIGNED_PACKAGES,
    GET_ASSIGNED_PACKAGES,
    GET_AVAILABLE_RIDERS,
    PACKAGE_LOADING,
    GET_ALERTS
} from './types';

let getBranchId = null;

// Get package by id
export const getPackageById = packageid => dispatch => {
    //dispatch(setPackageLoading());

    axios
        .get(`/package-manager?packageId=${packageid}`)
        .then(res =>
            dispatch({
                type: GET_PACKAGE,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PACKAGE,
                payload: null
            })
        );
};

// Get all packages
export const getPackages = () => dispatch => {
    dispatch(setPackageLoading());
    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/package?$limit=null&branchId=${getBranchId}`)
                .then(res =>
                    dispatch({
                        type: GET_PACKAGES,
                        payload: res.data
                    })
                )
                .catch(err =>
                    dispatch({
                        type: GET_PACKAGES,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_PACKAGES,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get('/package?$limit=null')
            .then(res =>
                dispatch({
                    type: GET_PACKAGES,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_PACKAGES,
                    payload: null
                })
            );
    }
};

// Get all assigned packages
export const getAssignedPackages = () => dispatch => {
    dispatch(setPackageLoading());
    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/package?status[$in]=Assigned&branchId=${getBranchId}`)
                .then(res => {
                    dispatch({
                        type: GET_ASSIGNED_PACKAGES,
                        payload: res.data
                    })
                })
                .catch(err =>
                    dispatch({
                        type: GET_ASSIGNED_PACKAGES,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_ASSIGNED_PACKAGES,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get('/package?status[$in]=Assigned')
            .then(res => {
                dispatch({
                    type: GET_ASSIGNED_PACKAGES,
                    payload: res.data
                })
            })
            .catch(err =>
                dispatch({
                    type: GET_ASSIGNED_PACKAGES,
                    payload: null
                })
            );
    }
};

// Get all unassigned packages
export const getUnassignedPackages = () => dispatch => {
    dispatch(setPackageLoading());
    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/package?status[$nin]=Cancelled&status[$nin]=Delivered&status[$nin]=Assigned&branchId=${getBranchId}`)
                .then(res =>
                    dispatch({
                        type: GET_UNASSIGNED_PACKAGES,
                        payload: res.data
                    })
                )
                .catch(err =>
                    dispatch({
                        type: GET_UNASSIGNED_PACKAGES,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_UNASSIGNED_PACKAGES,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get('/package?status[$nin]=Cancelled&status[$nin]=Delivered&status[$nin]=Assigned')
            .then(res =>
                dispatch({
                    type: GET_UNASSIGNED_PACKAGES,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_UNASSIGNED_PACKAGES,
                    payload: null
                })
            );
    }
};

// Get all available riders
export const getAvailableRiders = () => dispatch => {
    dispatch(setPackageLoading());
    axios
        .get('/profile-detail?role[$in]=rider')
        .then(res =>
            dispatch({
                type: GET_AVAILABLE_RIDERS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_AVAILABLE_RIDERS,
                payload: null
            })
        );
};

// package loading
export const setPackageLoading = () => {
    return {
        type: PACKAGE_LOADING
    };
};
