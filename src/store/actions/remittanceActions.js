import axios from 'axios';
import store from '../Store';

import {
    GET_BRANCH_REMITTANCES,
    GET_RIDER_REMITTANCES,
    REMITTANCE_LOADING,
    RECEIVE_REMITTANCE,
    GET_ALERTS
} from './types';

let getBranchId = null;

// Accept order
export const receiveRemittance = (data, trackingNo) => dispatch => {
    //dispatch(setOrderLoading());

    if (localStorage.userRole === 'super-admin') {
        axios
            .patch(`/remittance?trackingNumber=${trackingNo}`, data)
            .then(res => {
                dispatch({
                    type: RECEIVE_REMITTANCE,
                    payload: res.data
                });
                // dispatch({
                //     type: GET_BRANCH_REMITTANCES,
                //     payload: res.data
                // });
                // dispatch({
                //     type: GET_RIDER_REMITTANCES,
                //     payload: res.data
                // });
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Remittance has been received!!'
                    }
                });
            }
            )
            .catch(err => {
                dispatch({
                    type: RECEIVE_REMITTANCE,
                    payload: null
                });
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 0,
                        msg: 'Error receiving remittance!!!'
                    }
                });
            });
    }
};
// Get branch remittance
export const getBranchRemittance = (branchid) => dispatch => {
    dispatch(setRemittanceLoading());

    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/remittance?isRemitted=false&branchId=${getBranchId}`)
                .then(res =>
                    dispatch({
                        type: GET_BRANCH_REMITTANCES,
                        payload: res.data
                    })
                )
                .catch(err =>
                    dispatch({
                        type: GET_BRANCH_REMITTANCES,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_BRANCH_REMITTANCES,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get(`/remittance?isRemitted=false&branchId=${branchid}`)
            .then(res =>
                dispatch({
                    type: GET_BRANCH_REMITTANCES,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_BRANCH_REMITTANCES,
                    payload: null
                })
            );
    }
};

// Get rider remittance
export const getRiderRemittance = (riderid) => dispatch => {
    dispatch(setRemittanceLoading());

    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/remittance?isRemitted=false&riderId=${riderid}&branchId=
            ${getBranchId}`)
                .then(res =>
                    dispatch({
                        type: GET_RIDER_REMITTANCES,
                        payload: res.data
                    })
                )
                .catch(err =>
                    dispatch({
                        type: GET_RIDER_REMITTANCES,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_RIDER_REMITTANCES,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get(`/remittance?isRemitted=false&riderId=${riderid}`)
            .then(res =>
                dispatch({
                    type: GET_RIDER_REMITTANCES,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_RIDER_REMITTANCES,
                    payload: null
                })
            );
    }
};

// payment loading
export const setRemittanceLoading = () => {
    return {
        type: REMITTANCE_LOADING
    };
};
