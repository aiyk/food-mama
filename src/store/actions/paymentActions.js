import axios from 'axios';

import {
    GET_PAYMENT,
    GET_PAYMENTS,
    PAYMENT_LOADING
} from './types';

// Get food by id
export const getPaymentById = paymentid => dispatch => {
    dispatch(setPaymentLoading());

    axios
        .get(`/payment/${paymentid}`)
        .then(res =>
            dispatch({
                type: GET_PAYMENT,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PAYMENT,
                payload: null
            })
        );
};

// Get all payments
export const getPayments = () => dispatch => {
    dispatch(setPaymentLoading());
    axios
        .get('/cash-flow')
        .then(res =>
            dispatch({
                type: GET_PAYMENTS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PAYMENTS,
                payload: null
            })
        );
};

// payment loading
export const setPaymentLoading = () => {
    return {
        type: PAYMENT_LOADING
    };
};
