import axios from 'axios';

import {
    GET_BRANCH,
    GET_BRANCHES,
    GET_BRANCH_MANAGERS,
    BRANCH_LOADING,
    GET_ERRORS,
    GET_ALERTS
} from './types';

//Create branch
export const createBranch = (branchData, history) => dispatch => {
    axios
        .post('/branch', branchData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Branch successfully created!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating branch!!!'
                }
            });
        });
}

//Edit branch
export const editBranch = (branchData, history, branchid) => dispatch => {
    axios
        .patch(`/branch/${branchid}`, branchData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Branch successfully edited!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing branch!!!'
                }
            });
        });
}

// Get branch by id
export const getBranchById = branchid => dispatch => {
    dispatch(setBranchLoading());

    axios
        .get(`/api/branch/${branchid}`)
        .then(res =>
            dispatch({
                type: GET_BRANCH,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_BRANCH,
                payload: null
            })
        );
};

// Get all branches
export const getBranches = () => dispatch => {
    dispatch(setBranchLoading());
    axios
        .get('/branch')
        .then(res =>
            dispatch({
                type: GET_BRANCHES,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_BRANCHES,
                payload: null
            })
        );
};

// Get all branch managers
export const getBranchManagers = () => dispatch => {
    dispatch(setBranchLoading());
    axios
        .get('/profile-detail?role[$in]=branch-admin')
        .then(res =>
            dispatch({
                type: GET_BRANCH_MANAGERS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_BRANCH_MANAGERS,
                payload: null
            })
        );
};

// Delete branch
export const deleteBranch = () => dispatch => {
    if (window.confirm('Are you sure? This can NOT be undone!')) {
        axios
            .delete('/api/branch')
            .then(res => {
                dispatch({
                    type: GET_BRANCHES,
                    payload: null
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Branch successfully deleted!!!'
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 0,
                        msg: 'Error deleting branch!!!'
                    }
                });
            });
    }
};

// Branch loading
export const setBranchLoading = () => {
    return {
        type: BRANCH_LOADING
    };
};
