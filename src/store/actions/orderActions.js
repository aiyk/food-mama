import axios from 'axios';
import store from '../Store';

import {
    GET_ORDER,
    ACCEPT_ORDER,
    GET_ORDERS,
    GET_DELIVERED_ORDERS,
    GET_CONFIRMED_ORDERS,
    GET_IN_TRANSIT_ORDERS,
    GET_CANCELLED_ORDERS,
    GET_PENDING_ORDERS,
    GET_NEW_ORDERS,
    ORDER_LOADING,
    GET_ERRORS,
    GET_ALERTS
} from './types';

let getBranchId = null;

// Get order by id
export const getOrderById = orderid => dispatch => {
    //dispatch(setOrderLoading());

    axios
        .get(`/order-detail?orderId=${orderid}`)
        .then(res =>
            dispatch({
                type: GET_ORDER,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ORDER,
                payload: null
            })
        );
};

// Accept order
export const acceptOrder = (data, history) => dispatch => {
    //dispatch(setOrderLoading());

    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            data.branchId = getBranchId;

            axios
                .post("/package-manager", data)
                .then(res => {
                    dispatch({
                        type: ACCEPT_ORDER,
                        payload: res.data
                    })
                    history.push('/dashboard')
                    dispatch({
                        type: GET_ALERTS,
                        payload: {
                            type: 1,
                            msg: 'Order Accepted!!!'
                        }
                    });
                }
                )
                .catch(err => {
                    dispatch({
                        type: ACCEPT_ORDER,
                        payload: null
                    })
                    dispatch({
                        type: GET_ALERTS,
                        payload: {
                            type: 0,
                            msg: 'Error accepting order!!!'
                        }
                    });
                });
        } else {
            dispatch({
                type: ACCEPT_ORDER,
                payload: null
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error accepting order!!!'
                }
            });
        }
    }
};

// Get all order
export const getOrders = () => dispatch => {
    dispatch(setOrderLoading());

    axios
        .get('/order?$limit=null&orderStatus[$nin]=Created&$sort[createdAt]=-1')
        .then(res =>
            dispatch({
                type: GET_ORDERS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ORDERS,
                payload: null
            })
        );
};

// Get all new orders
export const getNewOrders = () => dispatch => {
    dispatch(setOrderLoading());
    axios
        .get('/order?orderStatus[$in]=Ordered&$sort[createdAt]=-1')
        .then(res =>
            dispatch({
                type: GET_NEW_ORDERS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_NEW_ORDERS,
                payload: null
            })
        );
};

// Get all pending orders
export const getPendingOrders = () => dispatch => {
    dispatch(setOrderLoading());
    axios
        .get('/order?$sort[createdAt]=-1')
        .then(res =>
            dispatch({
                type: GET_PENDING_ORDERS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PENDING_ORDERS,
                payload: null
            })
        );
};

// Get all confirmed orders
export const getConfirmedOrders = () => dispatch => {
    dispatch(setOrderLoading());

    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/order?orderStatus[$in]=Confirmed&branchId=${getBranchId}&$sort[createdAt]=-1`)
                .then(res =>
                    dispatch({
                        type: GET_CONFIRMED_ORDERS,
                        payload: res.data
                    })
                )
                .catch(err =>
                    dispatch({
                        type: GET_CONFIRMED_ORDERS,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_CONFIRMED_ORDERS,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get('/order?orderStatus[$in]=Confirmed')
            .then(res =>
                dispatch({
                    type: GET_CONFIRMED_ORDERS,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_CONFIRMED_ORDERS,
                    payload: null
                })
            );
    }
};

// Get all in transit orders
export const getInTransit = () => dispatch => {
    dispatch(setOrderLoading());

    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) { 
            axios
                .get(`/order?orderStatus[$in]=In-transit&branchId=${getBranchId}&$sort[createdAt]=-1`)
                .then(res => {
                    console.log(res);
                    dispatch({
                        type: GET_IN_TRANSIT_ORDERS,
                        payload: res.data
                    })
                }
                )
                .catch(err =>
                    dispatch({
                        type: GET_IN_TRANSIT_ORDERS,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_IN_TRANSIT_ORDERS,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get('/order?orderStatus[$in]=In-transit&$sort[createdAt]=-1')
            .then(res =>
                dispatch({
                    type: GET_IN_TRANSIT_ORDERS,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_IN_TRANSIT_ORDERS,
                    payload: null
                })
            );
    }
};

// Get all delivered orders
export const getDeliveredOrders = () => dispatch => {
    dispatch(setOrderLoading());

    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/order?orderStatus[$in]=Delivered&branchId=${getBranchId}&$sort[createdAt]=-1`)
                .then(res =>
                    dispatch({
                        type: GET_DELIVERED_ORDERS,
                        payload: res.data
                    })
                )
                .catch(err =>
                    dispatch({
                        type: GET_DELIVERED_ORDERS,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_DELIVERED_ORDERS,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get('/order?orderStatus[$in]=Delivered&$sort[createdAt]=-1')
            .then(res =>
                dispatch({
                    type: GET_DELIVERED_ORDERS,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_DELIVERED_ORDERS,
                    payload: null
                })
            );
    }
};

// Get all cancelled orders
export const getCancelledOrders = () => dispatch => {
    dispatch(setOrderLoading());

    if (localStorage.userRole === 'branch-admin' && store.getState().auth.branch.data) {
        if (store.getState().auth.branch.data.length > 0) {
            getBranchId = store.getState().auth.branch.data[0]._id;
        }
        if (getBranchId) {
            axios
                .get(`/order?$limit=null&orderStatus[$in]=Delivered&branchId=${getBranchId}&$sort[createdAt]=-1`)
                .then(res =>
                    dispatch({
                        type: GET_CANCELLED_ORDERS,
                        payload: res.data
                    })
                )
                .catch(err =>
                    dispatch({
                        type: GET_CANCELLED_ORDERS,
                        payload: null
                    })
                );
        } else {
            dispatch({
                type: GET_CANCELLED_ORDERS,
                payload: null
            })
        }
    } else if (localStorage.userRole === 'super-admin') {
        axios
            .get('/order?$limit=null&orderStatus[$in]=Delivered&$sort[createdAt]=-1')
            .then(res =>
                dispatch({
                    type: GET_CANCELLED_ORDERS,
                    payload: res.data
                })
            )
            .catch(err =>
                dispatch({
                    type: GET_CANCELLED_ORDERS,
                    payload: null
                })
            );
    }
};

// order loading
export const setOrderLoading = () => {
    return {
        type: ORDER_LOADING
    };
};
