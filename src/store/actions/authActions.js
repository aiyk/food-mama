import axios from 'axios';
import setAuthToken from '../../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import { GET_ALERTS, GET_ERRORS, SET_CURRENT_USER, SET_CURRENT_BRANCH } from './types';
import { getAnalytics } from './settingsActions';

//Login --get user token
export const loginUser = userData => dispatch => {
    axios
        .post('/authentication', userData)
        .then(res => {
            // Save to localStorage
            const { profile, accessToken } = res.data;
            // Set token to ls
            localStorage.setItem('jwtToken', accessToken);
            //set user data to ls
            const username = profile.firstName + ' ' + profile.lastName;
            localStorage.setItem('userName', username);
            localStorage.setItem('userRole', profile.role);
            localStorage.setItem('profileId', profile._id);
            // Set token to Auth header
            setAuthToken(accessToken);
            // Set current user
            dispatch(setCurrentUser(profile));
            // Set current branch
            dispatch(setCurrentBranch(profile._id));
            dispatch(getAnalytics(profile.role));
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Login Successful!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err
            });
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error logging in!!!'
                }
            });
        });
};

// Set logged in user
export const setCurrentUser = decoded => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    };
};

// Set logged in branch
export const setCurrentBranch = profileid => dispatch => {
    axios
        .get(`/branch?adminProfileId=${profileid}`)
        .then(res =>
            dispatch({
                type: SET_CURRENT_BRANCH,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: SET_CURRENT_BRANCH,
                payload: null
            })
        );
};

//log user out
export const logoutUser = () => dispatch => {
    //remove the token from localStorage
    localStorage.removeItem('jwtToken');
    // remove profile from ls
    localStorage.removeItem('userName');
    localStorage.removeItem('userRole');
    localStorage.removeItem('profileId');
    //remove auth header for future requests
    setAuthToken(false);
    //set current user to empty object and isAuthenticated to false
    dispatch(setCurrentUser({}));
    dispatch(setCurrentBranch({}));
    dispatch({
        type: GET_ALERTS,
        payload: {
            type: 1,
            msg: 'Logged Out!!!'
        }
    });
}