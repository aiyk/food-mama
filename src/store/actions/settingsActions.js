import axios from 'axios';

import {
    GET_ROLE,
    GET_ROLES,
    ROLE_LOADING,
    GET_ANALYTICS,

    GET_PROMO,
    GET_PROMOS,
    PROMO_LOADING,

    GET_CONFIG,
    GET_CONFIGS,
    CONFIG_LOADING,

    GET_WALLET,
    GET_WALLETS,
    WALLET_LOADING,

    GET_FOODCATEGORY,
    GET_FOODCATEGORIES,
    GET_MEALTYPES,
    FOODCATEGORY_LOADING,

    GET_FOOD_TYPES,
    FOOD_TYPE_LOADING,

    GET_MEMBERSHIP,
    GET_MEMBERSHIPS,
    MEMBERSHIP_LOADING,

    GET_ERRORS,
    GET_ALERTS,
    MEAL_LOADING
} from '../actions/types';

//Create role
export const createRole = (roleData, history) => dispatch => {
    axios
        .post('/user-role', roleData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'User role successfully created!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating user role!!!'
                }
            });
        });
}

//Edit role
export const editRole = (roleData, history, roleid) => dispatch => {
    axios
        .patch(`/user-role/${roleid}`, roleData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'User role successfully edited!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing user role!!!'
                }
            });
        });
}

// Get role by id
export const getRoleById = roleid => dispatch => {
    dispatch(setRoleLoading());

    axios
        .get(`/api/role/${roleid}`)
        .then(res =>
            dispatch({
                type: GET_ROLE,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ROLE,
                payload: null
            })
        );
};

// Get all roles
export const getRoles = () => dispatch => {
    dispatch(setRoleLoading());
    axios
        .get('/user-role')
        .then(res =>
            dispatch({
                type: GET_ROLES,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ROLES,
                payload: null
            })
        );
};

// Role loading
export const setRoleLoading = () => {
    return {
        type: ROLE_LOADING
    };
};

//Create Promo
export const createPromo = (promoData, history) => dispatch => {
    axios
        .post('/promo-code', promoData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Promo code successfully created!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating promo code!!!'
                }
            });
        });
}

//Edit promo
export const editPromo = (promoData, history, promoid) => dispatch => {
    axios
        .patch(`/promo-code/${promoid}`, promoData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Promo code successfully edited!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing promo code!!!'
                }
            });
        });
}

// Get promo by id
export const getPromoById = promoid => dispatch => {
    dispatch(setPromoLoading());

    axios
        .get(`/promo-code/${promoid}`)
        .then(res =>
            dispatch({
                type: GET_PROMO,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PROMO,
                payload: null
            })
        );
};

// Get all promos
export const getPromos = () => dispatch => {
    dispatch(setPromoLoading());
    axios
        .get('/promo-code')
        .then(res =>
            dispatch({
                type: GET_PROMOS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_PROMOS,
                payload: null
            })
        );
};

// Delete Promo
export const deletePromo = (_id) => dispatch => {
    if (window.confirm('Are you sure? This can NOT be undone!')) {
        axios
            .delete(`/promo-code/${_id}`)
            .then(res => {
                dispatch(getPromos());
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Successfully deleted promo code!!!'
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 0,
                        msg: 'Error deleting promo code!!!'
                    }
                });
            });
    }
};

// Promo loading
export const setPromoLoading = () => {
    return {
        type: PROMO_LOADING
    };
};

//Edit config
export const editConfig = (configData, history, configid) => dispatch => {
    axios
        .patch(`/config/${configid}`, configData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Configuration saved!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error saving configuration!!!'
                }
            });
        });
}

// Get all config
export const getConfigs = () => dispatch => {
    dispatch(setConfigLoading());
    axios
        .get('/config')
        .then(res =>
            dispatch({
                type: GET_CONFIGS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_CONFIGS,
                payload: null
            })
        );
};

// Config loading
export const setConfigLoading = () => {
    return {
        type: CONFIG_LOADING
    };
};

//Create wallet
export const createWallet = (walletData, history) => dispatch => {
    axios
        .post('/wallet', walletData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Wallet successfully created!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating wallet!!!'
                }
            });
        });
}

//Edit wallet
export const editWallet = (walletData, history, walletid) => dispatch => {
    axios
        .patch(`/wallet-type/${walletid}`, walletData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Wallet successfully edited!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing wallet!!!'
                }
            });
        });
}

// Get wallet by id
export const getWalletById = walletid => dispatch => {
    dispatch(setWalletLoading());

    axios
        .get(`/api/wallet/${walletid}`)
        .then(res =>
            dispatch({
                type: GET_WALLET,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_WALLET,
                payload: null
            })
        );
};

// Get all wallets
export const getWallets = () => dispatch => {
    dispatch(setWalletLoading());
    axios
        .get('/wallet-type')
        .then(res =>
            dispatch({
                type: GET_WALLETS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_WALLETS,
                payload: null
            })
        );
};

// Delete Wallet
export const deleteWallet = () => dispatch => {
    if (window.confirm('Are you sure? This can NOT be undone!')) {
        axios
            .delete('/api/wallet')
            .then(res => {
                dispatch({
                    type: GET_WALLETS,
                    payload: null
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Wallet successfully deleted!!!'
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 0,
                        msg: 'Error dedleting wallet!!!'
                    }
                });
            });
    }
};

// wallet loading
export const setWalletLoading = () => {
    return {
        type: WALLET_LOADING
    };
};

//Create food category
export const createFoodCategory = (foodCategoryData, history) => dispatch => {
    axios
        .post('/food-category', foodCategoryData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Food category successfully created!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating food category!!!'
                }
            });
        });
}

//Edit food category
export const editFoodCategory = (foodCategoryData, history, foodCategoryid) => dispatch => {
    axios
        .patch(`/food-category/${foodCategoryid}`, foodCategoryData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Food category successfully edited!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing food category!!!'
                }
            });
        });
}

// Get food category by id
export const getFoodCategoryById = foodCategoryid => dispatch => {
    dispatch(setFoodCategoryLoading());

    axios
        .get(`/api/foodcategory/${foodCategoryid}`)
        .then(res =>
            dispatch({
                type: GET_FOODCATEGORY,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_FOODCATEGORY,
                payload: null
            })
        );
};

// Get all food category
export const getFoodCategories = () => dispatch => {
    dispatch(setFoodCategoryLoading());
    axios
        .get('/food-category?$limit=null&$sort[createdAt]=-1')
        .then(res =>
            dispatch({
                type: GET_FOODCATEGORIES,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_FOODCATEGORIES,
                payload: null
            })
        );
};

// Get all meal types
export const getMeals = () => dispatch => {
    dispatch(setMealLoading());
    axios
        .get('/meals?$limit=null&$sort[createdAt]=-1')
        .then(res => {
            dispatch({
                type: GET_MEALTYPES,
                payload: res.data
            })
        }
        )
        .catch(err =>
            dispatch({
                type: GET_MEALTYPES,
                payload: null
            })
        );
};

// Delete food category
export const deleteFoodCategory = (_id) => dispatch => {
    if (window.confirm('Are you sure? This can NOT be undone!')) {
        axios
            .delete(`/food-category/${_id}`)
            .then(res => {
                dispatch(getFoodCategories())
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Food category successfully deleted!!!'
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 0,
                        msg: 'Error deleting food category!!!'
                    }
                });
            });
    }
};

// food category loading
export const setFoodCategoryLoading = () => {
    return {
        type: FOODCATEGORY_LOADING
    };
};

// meal loading
export const setMealLoading = () => {
    return {
        type: MEAL_LOADING
    };
};

//Create MEMBERSHIP
export const createMembership = (membershipData, history) => dispatch => {
    axios
        .post('/membership-plan', membershipData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Membership successfully created!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating membership!!!'
                }
            });
        });
}

//Edit membership
export const editMembership = (membershipData, history, membershipid) => dispatch => {
    axios
        .patch(`/membership-plan/${membershipid}`, membershipData)
        .then(res => {
            history.push('/dashboard')
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Membership successfully edited!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing membershhip!!!'
                }
            });
        });
}

// Get membership by id
export const getMembershipById = membershipid => dispatch => {
    dispatch(setMembershipLoading());

    axios
        .get(`/membership-plan/${membershipid}`)
        .then(res =>
            dispatch({
                type: GET_MEMBERSHIP,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_MEMBERSHIP,
                payload: null
            })
        );
};

// Get all memberships
export const getMemberships = () => dispatch => {
    dispatch(setMembershipLoading());
    axios
        .get('/membership-plan')
        .then(res =>
            dispatch({
                type: GET_MEMBERSHIPS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_MEMBERSHIPS,
                payload: null
            })
        );
};

// Delete membership
export const deleteMembership = () => dispatch => {
    if (window.confirm('Are you sure? This can NOT be undone!')) {
        axios
            .delete('/membership-plan')
            .then(res => {
                dispatch({
                    type: GET_MEMBERSHIPS,
                    payload: null
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Successfully Deleted membership!!!'
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 0,
                        msg: 'Error deleting membership!!!'
                    }
                });
            });
    }
};

// membership loading
export const setMembershipLoading = () => {
    return {
        type: MEMBERSHIP_LOADING
    };
};



//Create MEAL
export const createMeal = (mealData, history) => dispatch => {
    axios
        .post('/meals', mealData)
        .then(res => {
            dispatch(getMeals())
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Meal created successfully!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error creating meal!!!'
                }
            });
        });
}
export const editMeal = (mealData, mealId) => dispatch => {
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    axios
        .patch(`/meals/${mealId}`, mealData, config)
        .then(res => {
            dispatch(getMeals())
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 1,
                    msg: 'Meal created successfully!!!'
                }
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
            dispatch({
                type: GET_ALERTS,
                payload: {
                    type: 0,
                    msg: 'Error editing meal!!!'
                }
            });
        });
}

// membership loading
export const setFoodTypeLoading = () => {
    return {
        type: FOOD_TYPE_LOADING
    };
};

// Get food types
export const getFoodTypes = () => dispatch => {
    dispatch(setFoodTypeLoading());
    axios
        .get('/food-type?$limit=null&$sort[createdAt]=-1')
        .then(res =>
            dispatch({
                type: GET_FOOD_TYPES,
                payload: res.data
            })
        )
        .catch(err =>
            console.log(err)
        );
};

export const createFoodType = (body) => dispatch => {
    dispatch(setFoodTypeLoading());
    axios
        .post('/food-type', body)
        .then(res =>
            dispatch(getFoodTypes())
        )
        .catch(err =>
            dispatch({
                // type: GET_FOOD_TYPES,
                // payload: null
            })
        );
};

export const editFoodType = (body, foodTypeId) => dispatch => {
    dispatch(setFoodTypeLoading());
    axios
        .patch(`/food-type?_id=${foodTypeId}`, body)
        .then(res =>
            dispatch(getFoodTypes())
        )
        .catch(err => {
            console.log(JSON.stringify(err))
            dispatch(getFoodTypes())
        }
        );
};


// Delete food
export const deleteMeal = (_id) => dispatch => {
    if (window.confirm("Are you sure? This can NOT be undone!")) {
        axios
            .delete(`/meals/${_id}`)
            .then(res => {
                dispatch(getMeals());
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Meal successfully deleted!!!'
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
                dispatch({
                    type: GET_ALERTS,
                    payload: {
                        type: 1,
                        msg: 'Error deleting meal!!!'
                    }
                });
            });
    }
};

export const systemAlert = (msg) => dispatch =>
    dispatch({
        type: GET_ALERTS,
        payload: {
            type: 1,
            msg
        }
    });


// Get Analytics
export const getAnalytics = (role) => dispatch => {
    dispatch(setFoodTypeLoading());
    axios
        .get(`/analytics/${role}`)
        .then(res =>
            dispatch({
                type: GET_ANALYTICS,
                payload: res.data
            })
        )
        .catch(err =>
            console.log(err)
        );
};