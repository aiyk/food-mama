import axios from 'axios';

import {
  GET_USER,
  GET_USERS,
  GET_USER_ROLES,
  USER_LOADING,
  CLEAR_CURRENT_USER,
  GET_ERRORS,
  SET_CURRENT_USER,
  GET_ALERTS
} from './types';

//Create user
export const createUser = (userData, history) => dispatch => {
  axios
    .post('/sign-up', userData)
    .then(res => {
      history.push('/dashboard')
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 1,
          msg: 'User successfully created!!!'
        }
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 0,
          msg: 'Error creating user!!!'
        }
      });
    });
}

//Edit user
export const editUser = (userData, history, userid) => dispatch => {
  axios
    .patch(`/profile-detail/${userid}`, userData)
    .then(res => {
      history.push('/dashboard')
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 1,
          msg: 'User successfully edited!!!'
        }
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: 0,
          msg: 'Error editing user!!!'
        }
      });
    });
}

// Get current user
export const getCurrentUser = () => dispatch => {
  dispatch(setUserLoading());

  axios
    .get('/api/user')
    .then(res =>
      dispatch({
        type: GET_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_USER,
        payload: {}
      })
    );
};
// Get user by id
export const getUserById = userid => dispatch => {
  dispatch(setUserLoading());

  axios
    .get(`/api/user/${userid}`)
    .then(res =>
      dispatch({
        type: GET_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_USER,
        payload: null
      })
    );
};

// Get all users
export const getUsers = () => dispatch => {
  dispatch(setUserLoading());
  axios
    .get('/profile-detail?$limit=null')
    .then(res =>
      dispatch({
        type: GET_USERS,
        payload: res.data.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_USERS,
        payload: null
      })
    );
};

// Get user roles
export const getUserRoles = () => dispatch => {
  //dispatch(setUserLoading());
  axios
    .get('/user-role')
    .then(res =>
      dispatch({
        type: GET_USER_ROLES,
        payload: res.data.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_USER_ROLES,
        payload: null
      })
    );
};

// Delete account & USER
export const deleteAccount = () => dispatch => {
  if (window.confirm('Are you sure? This can NOT be undone!')) {
    axios
      .delete('/api/user')
      .then(res => {
        dispatch({
          type: SET_CURRENT_USER,
          payload: {}
        })
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 1,
            msg: 'User account deleted!!!'
          }
        });
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
        dispatch({
          type: GET_ALERTS,
          payload: {
            type: 0,
            msg: 'Error deleting user account!!!'
          }
        });
      });
  }
};

// User loading
export const setUserLoading = () => {
  return {
    type: USER_LOADING
  };
};

// Clear user
export const clearCurrentUser = () => {
  return {
    type: CLEAR_CURRENT_USER
  };
};
