import {
    GET_CONFIG,
    GET_CONFIGS,
    CONFIG_LOADING,
    GET_ANALYTICS
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "Configurations",
        tblSubtitle: "App Config Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: false,
                label: 'Create Config',
            }
        },
        trCheckbox: false,
        tblSummary: "Referal config currently available on this platform"
    },

    collections: null,
    config: null,
    loading: false,
    analytics: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case CONFIG_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_CONFIG:
            return {
                ...state,
                config: action.payload,
                loading: false
            };
        case GET_CONFIGS:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        case GET_ANALYTICS:
            return {
                ...state,
                analytics: action.payload
            }
        default:
            return state;
    }
}
