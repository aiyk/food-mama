import {
    GET_FOODCATEGORY,
    GET_FOODCATEGORIES,
    FOODCATEGORY_LOADING,
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "Food Categories",
        tblSubtitle: "Food Categories Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Create Food Category',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all food categories currently available on this platform"
    },

    collections: null,
    foodcategory: null,

    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FOODCATEGORY_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_FOODCATEGORY:
            return {
                ...state,
                foodcategory: action.payload,
                loading: false
            };
        case GET_FOODCATEGORIES:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
