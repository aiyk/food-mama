import { SET_CURRENT_USER, SET_CURRENT_BRANCH, GET_USER } from '../actions/types';
import isEmpty from '../../validation/is-empty';

const initState = {
    isAuthenticated: false,
    user: {},
    branch: {}
}

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload
            }
        case SET_CURRENT_BRANCH:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                branch: action.payload
            }
        case GET_USER:
            return {
                ...state,
                user: action.payload,
                loading: false
            };
        default:
            return state;
    }
}

export default authReducer