import {
    GET_ROLE,
    GET_ROLES,
    ROLE_LOADING
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "User Roles",
        tblSubtitle: "User Roles Management Platform",
        trActions: false,
        buttons: {
            AddNew: {
                isActive: false,
                label: 'Create Role',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all roles currently available on this platform"
    },

    collections: null,
    role: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ROLE_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_ROLE:
            return {
                ...state,
                role: action.payload,
                loading: false
            };
        case GET_ROLES:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
