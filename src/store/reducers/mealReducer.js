import {
    MEAL_LOADING,
    GET_MEALTYPES,
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "Meals",
        tblSubtitle: "Meal Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Create Meal',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all meals currently available on this platform"
    },

    collections: null,
    meal: null,

    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case MEAL_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_MEALTYPES:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
