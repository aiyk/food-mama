import {
    GET_MEMBERSHIP,
    GET_MEMBERSHIPS,
    MEMBERSHIP_LOADING
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "Membership Plan",
        tblSubtitle: "Membership Plan Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: false,
                label: 'Create Membership Plan',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all membership plan currently available on this platform"
    },

    collections: null,
    membership: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {

        case MEMBERSHIP_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_MEMBERSHIP:
            return {
                ...state,
                membership: action.payload,
                loading: false
            };
        case GET_MEMBERSHIPS:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
