import {
    GET_ORDER,
    GET_ORDERS,
    GET_DELIVERED_ORDERS,
    GET_CONFIRMED_ORDERS,
    GET_IN_TRANSIT_ORDERS,
    GET_CANCELLED_ORDERS,
    GET_PENDING_ORDERS,
    GET_NEW_ORDERS,
    ACCEPT_ORDER,
    ORDER_LOADING
} from '../actions/types';

const initialState = {
    metaData: {
        tblTitle: "Order Management",
        tblSubtitle: "Food Orders Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: false,
                label: null,
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all the orders placed on this platform"
    },

    collections: null,
    cancelledOrders: null,
    pendingOrders: null,
    newOrders: null,
    deliveredOrders: null,
    confirmedOrders: null,
    inTransitOrders: null,
    order: null,
    orderDetail: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ORDER_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_ORDER:
            return {
                ...state,
                orderDetail: action.payload,
                loading: false
            };
        case ACCEPT_ORDER:
            return {
                ...state,
                order: action.payload,
                loading: false
            };
        case GET_ORDERS:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        case GET_DELIVERED_ORDERS:
            return {
                ...state,
                deliveredOrders: action.payload,
                loading: false
            };
        case GET_CONFIRMED_ORDERS:
            return {
                ...state,
                confirmedOrders: action.payload,
                loading: false
            };
        case GET_IN_TRANSIT_ORDERS:
            return {
                ...state,
                inTransitOrders: action.payload,
                loading: false
            };
        case GET_CANCELLED_ORDERS:
            return {
                ...state,
                cancelledOrders: action.payload,
                loading: false
            };
        case GET_PENDING_ORDERS:
            return {
                ...state,
                pendingOrders: action.payload,
                loading: false
            };
        case GET_NEW_ORDERS:
            return {
                ...state,
                newOrders: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
