import {
    GET_FOOD,
    GET_FOODS,
    GET_BRANCH_FOODS,
    ADD_TO_BRANCH_FOODS,
    REMOVE_FROM_BRANCH_FOODS,
    GET_FOODCATEGORIES,
    FOOD_LOADING
} from '../actions/types';

const initialState = {
    metaData: {
        tblTitle: "Food Management",
        tblSubtitle: "Food Menu Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Create Food',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all food currently sold on this platform"
    },

    collections: null,
    branchFoods: null,
    food: null,
    foodDetail: null,
    foodCategories: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FOOD_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_FOOD:
            return {
                ...state,
                food: action.payload,
                loading: false
            };
        case GET_FOODS:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        case GET_BRANCH_FOODS:
            return {
                ...state,
                branchFoods: action.payload,
                loading: false
            };
        case GET_FOODCATEGORIES:
            return {
                ...state,
                foodCategories: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
