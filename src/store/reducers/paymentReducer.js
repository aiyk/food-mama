import {
    GET_PAYMENT,
    GET_PAYMENTS,
    PAYMENT_LOADING
} from '../actions/types';

const initialState = {
    metaData: {
        tblTitle: "Payments",
        tblSubtitle: "Payments Management Platform",
        trActions: false,
        buttons: {
            AddNew: {
                isActive: false,
                label: null,
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all transactions on this platform"
    },

    collections: null,
    payment: null,
    paymentDetail: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case PAYMENT_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_PAYMENT:
            return {
                ...state,
                payment: action.payload,
                loading: false
            };
        case GET_PAYMENTS:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
