import {
    GET_BRANCH,
    GET_BRANCHES,
    GET_BRANCH_MANAGERS,
    BRANCH_LOADING
} from '../actions/types';

const initialState = {
    metaData: {
        tblTitle: "Branch Management",
        tblSubtitle: "Registered Branches Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Create Branch',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all branches currently registered on this platform"
    },

    collections: null,
    branch: null,
    branchManagers: null,
    branchDetail: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case BRANCH_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_BRANCH:
            return {
                ...state,
                branch: action.payload,
                loading: false
            };
        case GET_BRANCHES:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        case GET_BRANCH_MANAGERS:
            return {
                ...state,
                branchManagers: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
