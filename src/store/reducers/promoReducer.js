import {
    GET_PROMO,
    GET_PROMOS,
    PROMO_LOADING
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "Promotions",
        tblSubtitle: "Promo Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Create Promo',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all promotions currently available on this platform"
    },

    collections: null,
    promo: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case PROMO_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_PROMO:
            return {
                ...state,
                promo: action.payload,
                loading: false
            };
        case GET_PROMOS:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
