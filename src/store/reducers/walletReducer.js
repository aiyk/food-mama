import {
    GET_WALLET,
    GET_WALLETS,
    WALLET_LOADING
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "Wallet Types",
        tblSubtitle: "Wallet Types Management Platform",
        trActions: false,
        buttons: {
            AddNew: {
                isActive: false,
                label: 'Create Wallet Type',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all wallet types currently available on this platform"
    },

    collections: null,
    wallet: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {

        case WALLET_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_WALLET:
            return {
                ...state,
                wallet: action.payload,
                loading: false
            };
        case GET_WALLETS:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
