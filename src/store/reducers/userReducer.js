import {
  GET_USER,
  GET_USERS,
  GET_USER_ROLES,
  USER_LOADING,
  CLEAR_CURRENT_USER
} from '../actions/types';

const initialState = {
  metaData: {
    tblTitle: "User Management",
    tblSubtitle: "System Administrators and Riders Management Platform",
    trActions: true,
    buttons: {
      AddNew: {
        isActive: true,
        label: 'Create User',
      }
    },
    trCheckbox: false,
    tblSummary: null
  },

  collections: null,
  user: null,
  userRoles: null,
  loading: false,
  usermenu: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case USER_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_USER:
      return {
        ...state,
        user: action.payload,
        loading: false
      };
    case GET_USERS:
      return {
        ...state,
        collections: action.payload,
        loading: false
      };
    case GET_USER_ROLES:
      return {
        ...state,
        userRoles: action.payload,
        loading: false
      };
    case CLEAR_CURRENT_USER:
      return {
        ...state,
        user: null
      };
    default:
      return state;
  }
}
