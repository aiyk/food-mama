import {
    GET_BRANCH_REMITTANCES,
    GET_RIDER_REMITTANCES,
    REMITTANCE_LOADING,
    RECEIVE_REMITTANCE
} from '../actions/types';

const initialState = {
    metaData: {
        tblTitle: "Remittances",
        tblSubtitle: "Remittances Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: false,
                label: null,
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all remittances on this platform"
    },

    remittance: null,
    branchRemittances: null,
    riderRemittances: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case REMITTANCE_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_RIDER_REMITTANCES:
            return {
                ...state,
                riderRemittances: action.payload,
                loading: false
            };
        case GET_BRANCH_REMITTANCES:
            return {
                ...state,
                branchRemittances: action.payload,
                loading: false
            };
        case RECEIVE_REMITTANCE:
            return {
                ...state,
                remittance: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
