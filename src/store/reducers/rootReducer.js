import authReducer from "./authReducer";
import alertReducer from "./alertReducer";
import errorReducer from "./errorReducer";
import userReducer from "./userReducer";
import enterpriseReducer from "./enterpriseReducer";
import branchReducer from "./branchReducer";
import foodReducer from "./foodReducer";
import membershipReducer from "./membershipReducer";
import orderReducer from "./orderReducer";
import paymentReducer from "./paymentReducer";
import remittanceReducer from "./remittanceReducer";
import packageReducer from "./packageReducer";
import dispatchReducer from "./dispatchReducer";
import { combineReducers } from "redux";
import foodCategoryReducer from "./foodCategoryReducer";
import roleReducer from "./roleReducer";
import promoReducer from "./promoReducer";
import configReducer from "./configReducer";
import walletReducer from "./walletReducer";
import MealReducer from './mealReducer';
import foodTypeReducer from "./foodTypeReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  alerts: alertReducer,
  errors: errorReducer,
  user: userReducer,
  food: foodReducer,
  foodCategory: foodCategoryReducer,
  role: roleReducer,
  promo: promoReducer,
  config: configReducer,
  wallet: walletReducer,
  enterprise: enterpriseReducer,
  branch: branchReducer,
  membership: membershipReducer,
  orders: orderReducer,
  payments: paymentReducer,
  remittances: remittanceReducer,
  packages: packageReducer,
  dispatchs: dispatchReducer,
  meal: MealReducer,
  foodTypes: foodTypeReducer
});

export default rootReducer;
