import {
    FOOD_TYPE_LOADING,
    GET_FOOD_TYPES,
} from '../actions/types';

const initialState = {

    metaData: {
        tblTitle: "Food Types",
        tblSubtitle: "Food Type Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Create Food Type',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all food types currently available on this platform"
    },

    collections: null,
    foodType: null,

    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FOOD_TYPE_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_FOOD_TYPES:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
