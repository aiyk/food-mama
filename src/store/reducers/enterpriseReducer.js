import {
    GET_ENTERPRISE,
    GET_ENTERPRISES,
    ENTERPRISE_LOADING
} from '../actions/types';

const initialState = {
    metaData: {
        tblTitle: "Enterprise Management",
        tblSubtitle: "Registered Enterprises Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Create Enterprise',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all enterprises currently registered on this platform"
    },

    collections: null,
    enterprise: null,
    enterpriseDetail: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ENTERPRISE_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_ENTERPRISE:
            return {
                ...state,
                enterprise: action.payload,
                loading: false
            };
        case GET_ENTERPRISES:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
