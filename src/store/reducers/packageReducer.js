import {
    GET_PACKAGE,
    GET_PACKAGES,
    GET_UNASSIGNED_PACKAGES,
    GET_ASSIGNED_PACKAGES,
    GET_AVAILABLE_RIDERS,
    PACKAGE_LOADING
} from '../actions/types';

const initialState = {
    metaData: {
        tblTitle: "Package Management",
        tblSubtitle: "Food Package Management Platform",
        trActions: true,
        buttons: {
            AddNew: {
                isActive: true,
                label: 'Assign Order To Rider',
            }
        },
        trCheckbox: false,
        tblSummary: "a list of all food packages"
    },

    collections: null,
    assignedPackages: null,
    unassignedPackages: null,
    availableRiders: null,
    package: null,
    packageDetail: null,
    loading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case PACKAGE_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_PACKAGE:
            return {
                ...state,
                packageDetail: action.payload,
                loading: false
            };
        case GET_PACKAGES:
            return {
                ...state,
                collections: action.payload,
                loading: false
            };
        case GET_UNASSIGNED_PACKAGES:
            return {
                ...state,
                unassignedPackages: action.payload,
                loading: false
            };
        case GET_ASSIGNED_PACKAGES:
            return {
                ...state,
                assignedPackages: action.payload,
                loading: false
            };
        case GET_AVAILABLE_RIDERS:
            return {
                ...state,
                availableRiders: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
