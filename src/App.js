import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./store/Store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import axiosGlobals from "./utils/axiosGlobals";
import {
  setCurrentUser,
  setCurrentBranch,
  logoutUser
} from "./store/actions/authActions";
import { clearCurrentUser } from "./store/actions/userActions";
import PrivateRoute from "./components/common/PrivateRoute";
import Alert from "./components/common/Alert";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getUsers } from "./store/actions/userActions";

import "./App.scss";
import MainNav from "./components/layout/MainNav";
import AppBar from "./components/layout/AppBar";
import Dashboard from "./components/dashboard/Dashboard";
import Login from "./components/auth/Login";
import Users from "./components/users/Users";
import Enterprises from "./components/enterprise/Enterprises";
import Foods from "./components/food/Foods";
import Orders from "./components/orders/Orders";
import Packages from "./components/packages/Packages";
import Payments from "./components/payments/Payments";
import Remittances from "./components/remittances/Remittances";
import Dispatch from "./components/dispatch/Dispatch";
import Branches from "./components/branches/Branches";
import Settings from "./components/settings/Settings";

//axios global config
axiosGlobals();

//check for token
if (localStorage.jwtToken) {
  //set token header auth
  setAuthToken(localStorage.jwtToken);
  //decode token and get user info and expiration
  const decoded = jwt_decode(localStorage.jwtToken);
  //set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  //check for branch data
  if (localStorage.profileId) {
    store.dispatch(setCurrentBranch(localStorage.profileId));
  }
  //check for expired token if (localStorage.userRole === 'super-admin') {
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    //logout user
    store.dispatch(logoutUser());
    //clear current user
    store.dispatch(clearCurrentUser());
    //reditrect to login
    window.location.href = "/login";
  }

  //check for user type
  if (
    localStorage.userRole === "branch-admin" ||
    localStorage.userRole === "super-admin"
  ) {
  } else {
    //logout user
    store.dispatch(logoutUser());
    //clear current user
    store.dispatch(clearCurrentUser());
    //reditrect to login
    window.location.href = "/login";
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleUserMenu: this.props.user.usermenu,
      notify: false
    };
  }
  componentDidMount() {
    this.props.getUsers();
  }
  componentDidUpdate(prevProps) {
    if (this.props.alerts !== prevProps.alerts) {
      this.setState({ notify: true });
      setTimeout(() => {
        this.setState({ notify: false });
      }, 2000);
    }
  }
  close_userMenu = $event => {
    if (
      $event.target.className === "loggedin-user" ||
      $event.target.className === "liu-img" ||
      $event.target.className === "liu-name" ||
      $event.target.className === "liuImg"
    ) {
    } else {
      this.setState({
        toggleUserMenu: false
      });
    }
    // console.log(this.state.toggleUserMenu, $event.target.className)
  };

  render() {
    return (
      <Router basename="/admin">
        <div onClick={this.close_userMenu} className="App">
          <MainNav />
          <div className="app-content-area">
            <div className="app-content-inwrap">
              <AppBar />
              <div className="content-body">
                <Switch>
                  <PrivateRoute exact path="/" component={Dashboard} />
                  <PrivateRoute exact path="/dashboard" component={Dashboard} />
                  <PrivateRoute exact path="/payments" component={Payments} />
                  <PrivateRoute
                    exact
                    path="/remittance"
                    component={Remittances}
                  />
                  <PrivateRoute exact path="/users" component={Users} />
                  <Route exact path="/login" component={Login} />
                  <PrivateRoute
                    exact
                    path="/enterprises"
                    component={Enterprises}
                  />
                  <PrivateRoute exact path="/foods" component={Foods} />
                  <PrivateRoute exact path="/branches" component={Branches} />
                  <PrivateRoute exact path="/orders" component={Orders} />
                  <PrivateRoute exact path="/packages" component={Packages} />
                  <PrivateRoute exact path="/dispatch" component={Dispatch} />
                  <PrivateRoute exact path="/settings" component={Settings} />
                </Switch>
              </div>
            </div>
          </div>

          <div className="alerts-container">
            {this.state.notify ? <Alert alerts={this.props.alerts} /> : null}
          </div>
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  getUsers: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  user: state.user,
  alerts: state.alerts
});

export default connect(
  mapStateToProps,
  { getUsers }
)(App);
