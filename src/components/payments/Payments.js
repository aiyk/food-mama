import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getPayments } from '../../store/actions/paymentActions'
import Table from '../common/Table'
import '../users/users.scss'

class Payments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openPopup: false
        }
    }

    componentDidMount() {
        this.props.getPayments();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        })
    }

    actionTrigger = (action, id) => {
        switch (action.toLowerCase()) {
            case 'detail':
                // this.props.getPackageById(id);
                break;
            default:
                break;
        }
    }
    render() {
        const {
            metaData,
            collections,
            loading
        } = this.props.payment;

        let data_remodel = [];
        let data_remodel_full = [];
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    Description: null,
                    Amount: null,
                    Type: null,
                    Channel: null,
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null,
                        img: null
                    }
                };

                key_val.Description = entry.paymentDescription;
                key_val.Type = entry.paymentType;
                key_val.Channel = entry.paymentChannel;
                key_val.Amount = entry.amount;
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;

                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    }
                );

                data_remodel.push(key_val);
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>{renderer}</div>
        )
    }
}

Payments.propTypes = {
    getPayments: PropTypes.func.isRequired,
    payment: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    payment: state.payments
});

export default connect(mapStateToProps, { getPayments })(Payments)