import React, { Component } from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from '../common/Spinner'
import { withRouter } from 'react-router-dom';
import { getAvailableRiders } from '../../store/actions/packageActions'
import { getRiderRemittance, receiveRemittance } from '../../store/actions/remittanceActions'
import Table from "../common/Table";

class RidersRemittances extends Component {
    state = {
        activeRider: null
    }
    componentDidMount() {
        this.props.getAvailableRiders();

        if (this.props.availableRiders === null) { } else {
            let rider = this.props.availableRiders.data[0];
            this.props.getRiderRemittance(rider._id);
            this.setState({ activeRider: rider._id });
        }
    }
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                //this.props.getOrderById(id);
                break;
            case "receive remittance":
                this.props.receiveRemittance({
                    amountReceived: item.Amount
                }, item.Tracker);
                break;
            default:
                break;
        }
    }
    getRiderRemittances = (riderid) => {
        this.props.getRiderRemittance(riderid);
        this.setState({ activeRider: riderid });
    }
    render() {
        const { metaData, riderRemittances, loading } = this.props.remittance;
        const { availableRiders } = this.props;

        let data_remodel = [];
        if (riderRemittances === null || this.props.remittance.loading) {
            //renderer = <Spinner />;
        } else {
            riderRemittances.data.forEach(function (entry) {

                let key_val = {
                    Tracker: null,
                    Amount: null,
                    Customer: {
                        Name: null,
                        Address: null
                    },
                    Rider: {
                        Name: null,
                        Mobile: null
                    },
                    Created: null,
                    id: null,
                    ignore: {
                        _id: null,
                        customer_id: null,
                        rider_id: null,
                        actions: []
                    }
                };

                key_val.Tracker = entry.trackingNumber;
                key_val.Amount = entry.package.order.totalCost;
                key_val.Customer.Name =
                    entry.package.order.customer.firstName + " " + entry.package.order.customer.lastName;
                key_val.Customer.Address =
                    entry.package.order.customer.address;
                key_val.Rider.Name =
                    entry.package.rider.firstName + " " + entry.package.rider.lastName;
                key_val.Rider.Mobile =
                    entry.package.rider.mobileNo;
                key_val.id = entry._id;
                key_val.Created = entry.createdAt;

                key_val.ignore.actions.push(
                    {
                        label: "Receive Remittance",
                        colorClass: "btn-outline-blue",
                        icon: "check.svg"
                    }
                );

                data_remodel.push(key_val);
            });
        }

        return (
            <div className='list-content-wrap'>
                <div className='lcw-list'>
                    <div className='lcs-title'>Riders</div>
                    {
                        availableRiders === null || this.props.loading
                            ? <Spinner />
                            : (
                                availableRiders.data.map(entry => {
                                    return <div
                                        key={entry._id}
                                        onClick={() => this.getRiderRemittances(entry._id)}
                                        className={'lcw-list-item ' + (this.state.activeRider === entry._id ? 'lcw-list-item-active' : null)}
                                    >{entry.firstName} {entry.lastName}</div>
                                })
                            )
                    }
                </div>
                <div className='lcw-content'>
                    {riderRemittances === null || this.props.remittance.loading
                        ? <Spinner />
                        : (
                            <Table
                                dataset={data_remodel}
                                dataset_full={null}
                                metaData={metaData}
                                dataDetail={null}
                                loading={this.props.remittance.loading}
                                callback={this.actionTrigger}
                                toggle_modal={this.toggle_modal}
                            />
                        )
                    }
                </div>
            </div>
        );
    }
}

RidersRemittances.propTypes = {
    getAvailableRiders: PropTypes.func.isRequired,
    getRiderRemittance: PropTypes.func.isRequired
}
const mapStateToProps = (state) => ({
    availableRiders: state.packages.availableRiders,
    remittance: state.remittances
});
export default connect(mapStateToProps, { getRiderRemittance, receiveRemittance, getAvailableRiders, withRouter })(RidersRemittances)
