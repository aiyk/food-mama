import React, { Component } from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from '../common/Spinner'
import { withRouter } from 'react-router-dom';
import { getBranches } from '../../store/actions/branchActions'
import { getBranchRemittance, receiveRemittance } from '../../store/actions/remittanceActions'
import Table from "../common/Table";

class BranchesRemittances extends Component {
    state = {
        activeBranch: null
    }
    componentDidMount() {
        this.props.getBranches();

        if (this.props.branch.collections === null) { } else {
            let branch = this.props.branch.collections.data[0]; 
            this.props.getBranchRemittance(branch._id);
            this.setState({ activeBranch: branch._id });
        }
    }
    actionTrigger = (action, id, item) => { 
        switch (action.toLowerCase()) {
            case "detail":
                //this.props.getOrderById(id);
                break;
            case "receive remittance":
                this.props.receiveRemittance({
                    amountReceived: item.Amount
                }, item.Tracker);
                break;
            default:
                break;
        }
    }
    getBranchRemittances = (branchid) => { 
        this.props.getBranchRemittance(branchid);
        this.setState({activeBranch: branchid});
    }
    render() {
        const { metaData, branchRemittances, loading } = this.props.remittance;
        const { branch } = this.props;

        let data_remodel = [];
        if (branchRemittances === null || this.props.remittance.loading) {
            //renderer = <Spinner />;
        } else {
            branchRemittances.data.forEach(function (entry) {

                let key_val = {
                    Tracker: null,
                    Amount: null,
                    Customer: {
                        Name: null,
                        Address: null
                    },
                    Rider: {
                        Name: null,
                        Mobile: null
                    },
                    Created: null,
                    id: null,
                    ignore: {
                        _id: null,
                        customer_id: null,
                        rider_id: null,
                        actions: []
                    }
                };

                key_val.Tracker = entry.trackingNumber;
                key_val.Amount = entry.package.order.totalCost;
                key_val.Customer.Name =
                    entry.package.order.customer.firstName + " " + entry.package.order.customer.lastName;
                key_val.Customer.Address =
                    entry.package.order.customer.address;
                key_val.Rider.Name =
                    entry.package.rider.firstName + " " + entry.package.rider.lastName;
                key_val.Rider.Mobile =
                    entry.package.rider.mobileNo;
                key_val.id = entry._id;
                key_val.Created = entry.createdAt;

                key_val.ignore.actions.push(
                    {
                        label: "Receive Remittance",
                        colorClass: "btn-outline-blue",
                        icon: "check.svg"
                    }
                );

                data_remodel.push(key_val);
            });
        }

        let renderer;

        return (
            <React.Fragment>
                {localStorage.userRole === 'super-admin'
                    ? (
                        <div className='list-content-wrap'>
                            <div className='lcw-list'>
                                <div className='lcs-title'>Branches</div>
                                {
                                    branch.collections === null || this.props.loading
                                        ? <Spinner />
                                        : (
                                            branch.collections.data.map( entry => {
                                                return <div
                                                    key={entry._id}
                                                    onClick={() => this.getBranchRemittances(entry._id)}
                                                    className={'lcw-list-item ' + (this.state.activeBranch === entry._id ? 'lcw-list-item-active' : null) }
                                                >{entry.name}</div>
                                            })
                                        )
                                }
                            </div>
                            <div className='lcw-content'>
                            {branchRemittances === null || this.props.remittance.loading
                                ? <Spinner />
                                : (
                                    <Table
                                        dataset={data_remodel}
                                        dataset_full={null}
                                        metaData={metaData}
                                        dataDetail={null}
                                        loading={loading}
                                        callback={this.actionTrigger}
                                        toggle_modal={this.toggle_modal}
                                    />
                                )
                            }
                            </div>
                        </div>
                    )
                    : null}

                {localStorage.userRole === 'branch-admin'
                    ? (
                        <Table
                            dataset={data_remodel}
                            dataset_full={null}
                            metaData={metaData}
                            dataDetail={null}
                            loading={loading}
                            callback={this.actionTrigger}
                            toggle_modal={this.toggle_modal}
                        />
                    )
                    : null}
            </React.Fragment>
        );
    }
}

BranchesRemittances.propTypes = {
    getBranches: PropTypes.func.isRequired,
    getBranchRemittance: PropTypes.func.isRequired,
    branch: PropTypes.object.isRequired
}
const mapStateToProps = (state) => ({
    branch: state.branch,
    remittance: state.remittances
});
export default connect(mapStateToProps, { getBranchRemittance, receiveRemittance, getBranches, withRouter })(BranchesRemittances)
