import React, { Component } from 'react'
import BranchesRemittances from "./BranchesRemittances";
import RidersRemittances from "./RidersRemittances";
import '../users/users.scss'

class Remittances extends Component {
    state = {
        openPopup: false,
        activeTab: "1"
    };
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        });
    };
    tab_click = tab => {
        this.setState({
            activeTab: tab
        });
    };

    actionTrigger = (action, id) => {
        switch (action.toLowerCase()) {
            case 'detail':
                // this.props.getPackageById(id);
                break;
            default:
                break;
        }
    }
    render() {

        let renderer;

        if (this.state.activeTab === "1") {
            renderer = (
                <BranchesRemittances />
            );
        }

        if (this.state.activeTab === "2") {
            renderer = (
                <RidersRemittances />
            )
        }

        return (
            <div>
                <div className="tab-wrap">
                    <ul className="tab-menu">
                        <li
                            onClick={() => this.tab_click("1")}
                            className={
                                "tab-menu-item " +
                                (this.state.activeTab === "1" ? "active-tab" : null)
                            }
                        >
                            Branhes Remittances
                        </li>
                        <li
                            onClick={() => this.tab_click("2")}
                            className={
                                "tab-menu-item " +
                                (this.state.activeTab === "2" ? "active-tab" : null)
                            }
                        >
                            Riders Remittances
                  </li>
                    </ul>
                    <div className="tab-content">{renderer}</div>
                </div>
            </div>
        );
    }
}

export default Remittances