import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createWallet, editWallet } from '../../store/actions/settingsActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup'
import TimesIco from '../../assets/icons/times.svg'

class CreateWalletType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Create New Wallet Type",
            walletType: '',
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        if (this.props.isEdit.active) {
            this.setState({
                walletType: this.props.isEdit.payload.Type
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const walletData = {
            wallet: this.state.walletType
        };

        if (this.props.isEdit.active) {
            this.props.editWallet(walletData, this.props.history, this.props.isEdit.payload.ignore._id);
        } else {
            this.props.createWallet(walletData, this.props.history);
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const { toggle_modal } = this.props;
        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='walletType'
                                value={this.state.walletType}
                                label='Wallet Type'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateWalletType.propTypes = {
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    wallet: state.walletTypes,
    errors: state.errors
});

export default connect(mapStateToProps, { createWallet, editWallet })(
    withRouter(CreateWalletType)
);