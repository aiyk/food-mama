import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getMemberships } from '../../store/actions/settingsActions'
import Table from '../common/Table'
import CreateMembership from './CreateMembership'
import '../users/users.scss'

class MembershipSettings extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getMemberships();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        })
    }
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "delete":
                break;
            default:
                break;
        }
    };
    render() {
        const {
            metaData,
            collections
        } = this.props.membership;
        const { loading } = this.props; console.log(collections);
        let data_remodel = [];
        let data_remodel_full = [];
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    Membership: null,
                    Criteria: null,
                    Benefit: null,
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null
                    }
                };

                key_val.Membership = entry.name;
                key_val.Criteria = entry.criteria;
                key_val.Benefit = entry.benefit;
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;

                key_val.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    }
                );

                data_remodel.push(key_val);
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createDispatch}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateMembership isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div>
        )
    }
}

MembershipSettings.propTypes = {
    getMemberships: PropTypes.func.isRequired,
    membership: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    membership: state.membership
});

export default connect(mapStateToProps, { getMemberships })(MembershipSettings)