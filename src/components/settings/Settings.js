import React, { Component } from 'react'
import './settings.scss'
import WalletSettings from './WalletSettings'
import RoleSettings from './RoleSettings'
import PromoSettings from './PromoSettings'
import ConfigSettings from './ConfigSettings'
import MembershipSettings from './MembershipSettings'
import FoodCategorySettings from './FoodCategorySettings'
import MealTypeSettings from './MealTypeSettings'
import FoodTypeSettings from './FoodTypeSettings'

class Settings extends Component {
    state = {
        config_tab_classes: ['tab-menu-item', 'active-tab'],
        role_tab_classes: ['tab-menu-item'],
        promo_tab_classes: ['tab-menu-item'],
        wallet_tab_classes: ['tab-menu-item'],
        foodCategory_tab_classes: ['tab-menu-item'],
        mealType_tab_classes: ['tab-menu-item'],
        foodType_tab_classes: ['tab-menu-item'],
        membership_tab_classes: ['tab-menu-item']
    }

    handleRoleClick = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item'],
            role_tab_classes: ['tab-menu-item', 'active-tab'],
            promo_tab_classes: ['tab-menu-item'],
            wallet_tab_classes: ['tab-menu-item'],
            foodCategory_tab_classes: ['tab-menu-item'],
            mealType_tab_classes: ['tab-menu-item'],
            foodType_tab_classes: ['tab-menu-item'],
            membership_tab_classes: ['tab-menu-item']
        });
    }
    handlePromoClick = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item'],
            role_tab_classes: ['tab-menu-item'],
            promo_tab_classes: ['tab-menu-item', 'active-tab'],
            wallet_tab_classes: ['tab-menu-item'],
            foodCategory_tab_classes: ['tab-menu-item'],
            mealType_tab_classes: ['tab-menu-item'],
            foodType_tab_classes: ['tab-menu-item'],
            membership_tab_classes: ['tab-menu-item']
        });
    }
    handleConfigClick = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item', 'active-tab'],
            role_tab_classes: ['tab-menu-item'],
            promo_tab_classes: ['tab-menu-item'],
            wallet_tab_classes: ['tab-menu-item'],
            foodCategory_tab_classes: ['tab-menu-item'],
            mealType_tab_classes: ['tab-menu-item'],
            foodType_tab_classes: ['tab-menu-item'],
            membership_tab_classes: ['tab-menu-item']
        });
    }
    handleWalletClick = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item'],
            role_tab_classes: ['tab-menu-item'],
            promo_tab_classes: ['tab-menu-item'],
            wallet_tab_classes: ['tab-menu-item', 'active-tab'],
            foodCategory_tab_classes: ['tab-menu-item'],
            mealType_tab_classes: ['tab-menu-item'],
            foodType_tab_classes: ['tab-menu-item'],
            membership_tab_classes: ['tab-menu-item']
        });
    }
    handleFoodCategoryClick = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item'],
            role_tab_classes: ['tab-menu-item'],
            promo_tab_classes: ['tab-menu-item'],
            wallet_tab_classes: ['tab-menu-item'],
            foodCategory_tab_classes: ['tab-menu-item', 'active-tab'],
            mealType_tab_classes: ['tab-menu-item'],
            foodType_tab_classes: ['tab-menu-item'],
            membership_tab_classes: ['tab-menu-item']
        });
    }
    handleMealTypeClick = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item'],
            role_tab_classes: ['tab-menu-item'],
            promo_tab_classes: ['tab-menu-item'],
            wallet_tab_classes: ['tab-menu-item'],
            foodCategory_tab_classes: ['tab-menu-item'],
            mealType_tab_classes: ['tab-menu-item', 'active-tab'],
            foodType_tab_classes: ['tab-menu-item'],
            membership_tab_classes: ['tab-menu-item']
        });
    }
    handleFoodTypeClick = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item'],
            role_tab_classes: ['tab-menu-item'],
            promo_tab_classes: ['tab-menu-item'],
            wallet_tab_classes: ['tab-menu-item'],
            foodCategory_tab_classes: ['tab-menu-item'],
            mealType_tab_classes: ['tab-menu-item'],
            foodType_tab_classes: ['tab-menu-item', 'active-tab'],
            membership_tab_classes: ['tab-menu-item']
        });
    }
    handleMembershipPlan = () => {
        this.setState({
            config_tab_classes: ['tab-menu-item'],
            role_tab_classes: ['tab-menu-item'],
            promo_tab_classes: ['tab-menu-item'],
            wallet_tab_classes: ['tab-menu-item'],
            foodCategory_tab_classes: ['tab-menu-item'],
            mealType_tab_classes: ['tab-menu-item'],
            foodType_tab_classes: ['tab-menu-item'],
            membership_tab_classes: ['tab-menu-item', 'active-tab']
        });
    }

    render() {
        let config_base = this.state.config_tab_classes[0];
        let config_active = '';
        let tab_content = '';

        if (this.state.config_tab_classes.length > 1) {
            config_active = this.state.config_tab_classes[1];
            tab_content = <ConfigSettings />;
        }

        let role_base = this.state.role_tab_classes[0];
        let role_active = '';

        if (this.state.role_tab_classes.length > 1) {
            role_active = this.state.role_tab_classes[1];
            tab_content = <RoleSettings />;
        }
        let promo_base = this.state.promo_tab_classes[0];
        let promo_active = '';

        if (this.state.promo_tab_classes.length > 1) {
            promo_active = this.state.promo_tab_classes[1];
            tab_content = <PromoSettings />;
        }

        let wallet_base = this.state.wallet_tab_classes[0];
        let wallet_active = '';
        if (this.state.wallet_tab_classes.length > 1) {
            wallet_active = this.state.wallet_tab_classes[1];
            tab_content = <WalletSettings />;
        }
        let foodCategory_base = this.state.foodCategory_tab_classes[0];
        let foodCategory_active = '';
        if (this.state.foodCategory_tab_classes.length > 1) {
            foodCategory_active = this.state.foodCategory_tab_classes[1];
            tab_content = <FoodCategorySettings />;
        }
        let mealType_base = this.state.mealType_tab_classes[0];
        let mealType_active = '';
        if (this.state.mealType_tab_classes.length > 1) {
            mealType_active = this.state.mealType_tab_classes[1];
            tab_content = <MealTypeSettings />;
        }
        let foodType_base = this.state.foodType_tab_classes[0];
        let foodType_active = '';
        if (this.state.foodType_tab_classes.length > 1) {
            foodType_active = this.state.foodType_tab_classes[1];
            tab_content = <FoodTypeSettings />;
        }
        let membership_base = this.state.membership_tab_classes[0];
        let membership_active = '';
        if (this.state.membership_tab_classes.length > 1) {
            membership_active = this.state.membership_tab_classes[1];
            tab_content = <MembershipSettings />;
        }

        return (
            <div>
                <div className="tab-menu">
                    <div onClick={this.handleConfigClick} className={config_base + ' ' + config_active}>Config</div>
                    {/* <div onClick={this.handleRoleClick} className={role_base + ' ' + role_active}>Role</div> */}
                    <div onClick={this.handlePromoClick} className={promo_base + ' ' + promo_active}>Promo</div>
                    {/* <div onClick={this.handleWalletClick} className={wallet_base + ' ' + wallet_active}>Wallet</div> */}
                    <div onClick={this.handleFoodCategoryClick} className={foodCategory_base + ' ' + foodCategory_active}>Food Category</div>
                    <div onClick={this.handleMealTypeClick} className={mealType_base + ' ' + mealType_active}>Meal Types</div>
                    <div onClick={this.handleFoodTypeClick} className={foodType_base + ' ' + foodType_active}>Food Types</div>
                    <div onClick={this.handleMembershipPlan} className={membership_base + ' ' + membership_active}>Membership Plan</div>
                </div>

                <div className="tab-content">{tab_content}</div>
            </div>
        )
    }
}

export default Settings