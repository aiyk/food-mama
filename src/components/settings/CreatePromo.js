import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createPromo, editPromo } from '../../store/actions/settingsActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup'
import CheckboxFieldGroup from '../common/CheckboxFieldGroup'
import TimesIco from '../../assets/icons/times.svg'

class CreatePromo extends Component {
    state = {
        modalTitle: "Create New Promo",
        promoCode: '',
        promoDescription: '',
        promoAmount: '',
        oneTime: false,
        isActive: false,
        errors: {}
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        if (this.props.isEdit.active) {
            let isActive = this.state.isActive;
            let oneTime = this.state.oneTime;

            if (this.props.isEdit.payload.Active == 'YES') {
                isActive = true;
            }
            if (this.props.isEdit.payload.OneTime == 'YES') {
                oneTime = true;
            }
            this.setState({
                promoCode: this.props.isEdit.payload.Code,
                promoDescription: this.props.isEdit.payload.Description,
                promoAmount: this.props.isEdit.payload.Amount,
                isActive: isActive,
                oneTime: oneTime,
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const promoData = {
            promoCode: this.state.promoCode,
            promoDescription: this.state.promoDescription,
            promoAmount: this.state.promoAmount,
            oneTime: this.state.oneTime,
            isActive: this.state.isActive,
        };
        if (this.props.isEdit.active) {
            this.props.editPromo(promoData, this.props.history, this.props.isEdit.payload.ignore._id);
        } else {
            this.props.createPromo(promoData, this.props.history);
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleOnetime = () => {
        this.setState({ oneTime: !this.state.oneTime });
    }
    handleIsactive = () => {
        this.setState({ isActive: !this.state.isActive });
    }
    render() {
        const { toggle_modal } = this.props;
        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='promoCode'
                                value={this.state.promoCode}
                                label='Promo Code'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='promoDescription'
                                value={this.state.promoDescription}
                                label='Promo Description'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='promoAmount'
                                value={this.state.promoAmount}
                                label='Promo Amount'
                                error={null}
                                type='number'
                                onChange={this.handleChange}
                            />
                            <CheckboxFieldGroup
                                name='oneTime'
                                checked={this.state.oneTime}
                                label='One Time'
                                onChange={this.handleOnetime}
                            />
                            <CheckboxFieldGroup
                                name='isActive'
                                checked={this.state.isActive}
                                label='Activate'
                                onChange={this.handleIsactive}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreatePromo.propTypes = {
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    promo: state.promos,
    errors: state.errors
});

export default connect(mapStateToProps, { createPromo, editPromo })(
    withRouter(CreatePromo)
);