import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getPromos, deletePromo } from '../../store/actions/settingsActions'
import Table from '../common/Table'
import CreatePromo from './CreatePromo'
import '../users/users.scss'

class PromoSettings extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getPromos();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        }, () => !this.state.openPopup && this.nullEditPayload())
    }
    nullEditPayload = () => this.setState({isEdit : {
        active: false,
        payload: null
    }})
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "delete":
                this.props.deletePromo(id);
                break;
            default:
                break;
        }
    };
    render() {
        const {
            metaData,
            collections
        } = this.props.promo;
        const { loading } = this.props;

        let data_remodel = [];
        let data_remodel_full = [];
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    Code: null,
                    Description: null,
                    Amount: null,
                    Active: '-',
                    OneTime: '-',
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null
                    }
                };

                key_val.Code = entry.promoCode;
                key_val.Description = entry.promoDescription;
                key_val.Amount = entry.promoAmount;
                if (entry.isActive) {
                    key_val.Active = 'YES';
                }
                if (entry.oneTime) {
                    key_val.OneTime = 'YES';
                }
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;

                key_val.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Delete",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );

                data_remodel.push(key_val);
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createDispatch}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreatePromo isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div>
        )
    }
}

PromoSettings.propTypes = {
    getPromos: PropTypes.func.isRequired,
    promo: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    promo: state.promo
});

export default connect(mapStateToProps, { getPromos, deletePromo })(PromoSettings)