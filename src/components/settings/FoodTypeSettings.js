import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getFoodTypes } from '../../store/actions/settingsActions'
import Table from '../common/Table'
import '../users/users.scss'
import CreateMealtype from './CreateMealtype'
import CreateFoodType from './CreateFoodType'

class FoodTypeSettings extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getFoodTypes();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        }, () => !this.state.openPopup && this.nullEditPayload())
    }
    nullEditPayload = () => this.setState({isEdit : {
        active: false,
        payload: null
    }})
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "delete":
                break;
            default:
                break;
        }
    };
    render() {
        const {
            metaData,
            collections
        } = this.props.foodTypes;
        const { loading } = this.props;

        let data_remodel = [];
        let data_remodel_full = [];
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    FoodType: null,
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null
                    }
                };

                key_val.FoodType = entry.foodType;
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;

                key_val.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Delete",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );

                data_remodel.push(key_val);
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createDispatch}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateFoodType isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div>
        )
    }
}

FoodTypeSettings.propTypes = {
    getFoodTypes: PropTypes.func.isRequired,
    meals: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    foodTypes: state.foodTypes,
    loading: state.foodTypes.loading
});

export default connect(mapStateToProps, { getFoodTypes })(FoodTypeSettings)