import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getConfigs } from '../../store/actions/settingsActions'
import Table from '../common/Table'
import CreateConfig from './CreateConfig'
import '../users/users.scss'

class ConfigSettings extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getConfigs();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        })
    }
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "delete":
                break;
            default:
                break;
        }
    };
    render() {
        const {
            metaData,
            collections
        } = this.props.config;
        const { loading } = this.props;

        let data_remodel = [];
        let data_remodel_full = [];
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    Type: null,
                    Percentage: null,
                    Amount: null,
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null
                    }
                };

                key_val.Type = entry.configType;
                key_val.Percentage = entry.configPercent;
                key_val.Amount = entry.configAmount;
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;

                key_val.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Delete",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );

                data_remodel.push(key_val);
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createDispatch}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateConfig isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div>
        )
    }
}

ConfigSettings.propTypes = {
    getConfigs: PropTypes.func.isRequired,
    config: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    config: state.config
});

export default connect(mapStateToProps, { getConfigs })(ConfigSettings)