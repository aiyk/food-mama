import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getMeals, deleteMeal } from '../../store/actions/settingsActions'
import Table from '../common/Table'
import '../users/users.scss'
import CreateMealtype from './CreateMealtype'

class MealTypeSettings extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getMeals();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        }, () => !this.state.openPopup && this.nullEditPayload())
    }
    nullEditPayload = () => this.setState({isEdit : {
        active: false,
        payload: null
    }})
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "delete":
                this.props.deleteMeal(id);
                break;
            default:
                break;
        }
    };
    render() {
        const {
            metaData,
            collections
        } = this.props.meal;
        const { loading } = this.props;

        let data_remodel = [];
        let data_remodel_full = [];
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    Meal: null,
                    Date: null,
                    id: null,
                    Description: null,
                    "Pre-Order": null,
                    Price: null,
                    ignore: {
                        actions: [],
                        categoryId: null
                    }
                };

                key_val.images = entry.image;
                key_val.Meal = entry.name;
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.Description = entry.description;
                key_val["Pre-Order"] = entry.preOrder;
                key_val.Price = entry.price
                key_val.ignore.categoryId = entry.categoryId;

                key_val.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Delete",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );

                data_remodel.push(key_val);
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createDispatch}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateMealtype isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div>
        )
    }
}

MealTypeSettings.propTypes = {
    getMeals: PropTypes.func.isRequired,
    meals: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    meal: state.meal
});

export default connect(mapStateToProps, { getMeals, deleteMeal })(MealTypeSettings)