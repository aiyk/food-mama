import React, { Component } from 'react'
import { connect } from 'react-redux';
import { editConfig } from '../../store/actions/settingsActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup'
import CheckboxFieldGroup from '../common/CheckboxFieldGroup'
import TimesIco from '../../assets/icons/times.svg'

class CreateConfig extends Component {
    state = {
        modalTitle: "Configuration",
        configType: '',
        configPercent: '',
        configAmount: '',
        errors: {}
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        if (this.props.isEdit.active) {
            this.setState({
                configType: this.props.isEdit.payload.Type,
                configPercent: this.props.isEdit.payload.Percentage,
                configAmount: this.props.isEdit.payload.Amount
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const configData = {
            configType: this.state.configType,
            configPercent: this.state.configPercent,
            configAmount: this.state.configAmount
        };
        if (this.props.isEdit.active) {
            this.props.editConfig(configData, this.props.history, this.props.isEdit.payload.ignore._id);
        } else {
            this.props.createPromo(configData, this.props.history);
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const { toggle_modal } = this.props;
        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='configType'
                                value={this.state.configType}
                                label='Configuration'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='configPercent'
                                value={this.state.configPercent}
                                label='Percentage'
                                error={null}
                                type='number'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='configAmount'
                                value={this.state.configAmount}
                                label='Amount'
                                error={null}
                                type='number'
                                onChange={this.handleChange}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateConfig.propTypes = {
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    config: state.config,
    errors: state.errors
});

export default connect(mapStateToProps, { editConfig })(
    withRouter(CreateConfig)
);