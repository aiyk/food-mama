import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createMeal, editFoodCategory } from '../../store/actions/settingsActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { createFoodType, editFoodType } from '../../store/actions/settingsActions'
import TextFieldGroup from '../common/TextFieldGroup'
import TimesIco from '../../assets/icons/times.svg'

class CreateFoodType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Create New Food Type",
            foodType: "",
            foodTypeId: "",
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        if (this.props.isEdit.active) {
            this.setState({
                foodType: this.props.isEdit.payload.FoodType,
                foodTypeId: this.props.isEdit.payload.id
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.props.isEdit.active) {
            this.props.editFoodType({ foodType: this.state.foodType }, this.state.foodTypeId);
        } else {
            this.props.createFoodType({ foodType: this.state.foodType });
        }
        this._toggle_modal();
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    _toggle_modal = () => {
        this.setState({
            foodType: "",
            foodTypeId: ""
        }, () => this.props.toggle_modal());
    }


    render() {
        const { toggle_modal } = this.props;
        return (
            <div className="modal-overlay" >
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={this._toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='foodType'
                                value={this.state.foodType}
                                label='Food Type'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateFoodType.propTypes = {
    foodCategory: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    errors: state.errors
});

export default connect(mapStateToProps, { createFoodType, editFoodType })(
    withRouter(CreateFoodType)
);