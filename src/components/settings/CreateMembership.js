import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createMembership, editMembership } from '../../store/actions/settingsActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup'
import TimesIco from '../../assets/icons/times.svg'

class CreateMembership extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Update Membership Plan",
            criteria: '',
            benefit: '',
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        if (this.props.isEdit.active) {
            this.setState({
                criteria: this.props.isEdit.payload.Criteria,
                benefit: this.props.isEdit.payload.Benefit
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const membershipData = {
            criteria: this.state.criteria,
            benefit: this.state.benefit
        };

        if (this.props.isEdit.active) {
            this.props.editMembership(membershipData, this.props.history, this.props.isEdit.payload.ignore._id);
        } else {
            this.props.createMembership(membershipData, this.props.history);
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const { toggle_modal } = this.props;
        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='criteria'
                                value={this.state.criteria}
                                label='Criteria'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='benefit'
                                value={this.state.benefit}
                                label='Benefit'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateMembership.propTypes = {
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    membership: state.membershipPlan,
    errors: state.errors
});

export default connect(mapStateToProps, { createMembership, editMembership })(
    withRouter(CreateMembership)
);