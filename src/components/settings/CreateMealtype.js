import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createMeal, editMeal, systemAlert } from '../../store/actions/settingsActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getFoodCategories, getMeals } from '../../store/actions/settingsActions'
import TextFieldGroup from '../common/TextFieldGroup'
import TimesIco from '../../assets/icons/times.svg'
import CheckboxFieldGroup from '../common/CheckboxFieldGroup';
import ImageUploader from "react-images-upload";
import SelectListGroup from '../common/SelectListGroup';
import axios from 'axios';

class CreateMealType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Create New Meal",
            createMeal: {
                images: [],
                categoryId: "",
                name: "",
                price: "",
                description: "",
                preOrder: false
            },
            mealTypeId: "",
            images: [],
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }


    componentDidMount() {
        this.props.getFoodCategories();
        if (this.props.isEdit.active) {
            const { Meal, id, Description, Price, images, ignore } = this.props.isEdit.payload;
            console.log('id', id);
            this.setState({
                createMeal: {
                    name: Meal,
                    preOrder: this.props.isEdit.payload['Pre-Order'],
                    price: Price,
                    description: Description,
                    categoryId: ignore.categoryId,
                    images: []
                },
                mealTypeId: id,
                images
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        let createMealData = new FormData();
        Object.keys(this.state.createMeal).map(it => {
            if (it == "images") {
                if (this.state.createMeal[it].length > 0) {
                    this.state.createMeal[it].map(image => createMealData.append(it, image));
                }
            } else {
                if (this.state.createMeal[it]) {
                    createMealData.append(it, this.state.createMeal[it]);
                }
            }
        });

        if (this.props.isEdit.active) {
            console.log('before edit', this.state.mealTypeId);
            this.props.editMeal(createMealData, this.state.mealTypeId);
        } else {
            this.props.createMeal(createMealData);
        }
    }
    handleChange = (e) => {
        this.setState({ createMeal: { ...this.state.createMeal, ...{ [e.target.name]: e.target.value } } });
    }
    handleCheck = () => {
        this.setState((prevState) => ({
            createMeal: { ...prevState.createMeal, preOrder: !prevState.createMeal.preOrder }
        }));
    };

    _onDrop = (pictureFiles, pictureDataURLs) => {
        this.setState({ createMeal: { ...this.state.createMeal, ...{ images: [...this.state.createMeal.images, ...pictureFiles] } } });
        // this.setState((prevState) => ({
        //     createMeal: { ...prevState.createMeal.images, ...pictureFiles }
        // }));
    }

    _toggle_modal = () => {
        this.setState({}, () => this.props.toggle_modal());
    }

    _deleteImage = async (imageName) => {
        let deleteImage = await axios.delete(`/manage-images/${imageName}?type=meal`);
        if (deleteImage) {
            let images = this.state.images.filter(img => img != imageName);
            this.setState({ images }, () => {
                this.props.getMeals();
                this.props.systemAlert("Image deleted successfully");
            });
        }
    }

    render() {
        const { toggle_modal, foodCategory } = this.props;
        let options = [{ label: "Select a category...", value: null }];
        options = foodCategory.collections ? [...options, ...foodCategory.collections.data.map(it => ({ label: it.name, value: it._id }))] : options;
        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={this._toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <div>
                                {this.state.images.map((image, index) =>
                                    <div>
                                        <img alt="" onClick={() => this._deleteImage(image)} src={TimesIco} />
                                        <img key={index * 232} src={`https://foodmama.ng/uploads/foodimage/${image}`} style={{ height: "100px", width: "100px" }} />
                                    </div>
                                )}
                            </div>
                            <TextFieldGroup
                                name='name'
                                value={this.state.createMeal.name}
                                label='Meal Name'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <SelectListGroup
                                name="categoryId"
                                value={this.state.createMeal.categoryId}
                                onChange={this.handleChange}
                                options={options}
                                error={null}
                                label="Category"
                            />
                            <CheckboxFieldGroup
                                name="preOrder"
                                checked={this.state.createMeal.preOrder}
                                label="Pre Order"
                                onChange={this.handleCheck}
                            />
                            <TextFieldGroup
                                name='description'
                                value={this.state.createMeal.description}
                                label='Description'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='price'
                                value={this.state.createMeal.price}
                                label='Price'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <ImageUploader
                                withIcon={true}
                                buttonText="Upload Meal Images"
                                onChange={this._onDrop}
                                withPreview={true}
                                imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                                maxFileSize={5242880}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateMealType.propTypes = {
    foodCategory: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    foodCategory: state.foodCategory,
    errors: state.errors
});

export default connect(mapStateToProps, { getFoodCategories, createMeal, editMeal, getMeals, systemAlert, getMeals })(
    withRouter(CreateMealType)
);