import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getEnterprises } from '../../store/actions/enterpriseActions'
import Table from '../common/Table'
import CreateEnterprise from './CreateEnterprise'
import '../users/users.scss'

class Enterprises extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getEnterprises();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        }, () => !this.state.openPopup && this.nullEditPayload())
    }
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                //this.props.getOrderById(id);
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "deactivate":
                break;
            default:
                break;
        }
    };

    nullEditPayload = () => this.setState({isEdit : {
        active: false,
        payload: null
    }})

    render() {
        const {
            metaData,
            collections,
            loading
        } = this.props.enterprise;

        let data_remodel = [];
        let data_remodel_full = [];
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    Enterprise: null,
                    Phone: null,
                    ["Enterprise Email"]: null,
                    ["Enterprise Admin Email"]: null,
                    Address: null,
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null
                    }
                };

                key_val.Enterprise = entry.name;
                key_val.Phone = entry.mobile;
                key_val["Enterprise Email"] = entry.enterpriseEmail;
                key_val["Enterprise Admin Email"] = entry.enterpriseAdminEmail;
                key_val.Address = entry.address;
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;

                key_val.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Deactivate",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );

                data_remodel.push(key_val);
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createEnterprise}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateEnterprise isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div>
        )
    }
}

Enterprises.propTypes = {
    getEnterprises: PropTypes.func.isRequired,
    enterprise: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    enterprise: state.enterprise
});

export default connect(mapStateToProps, { getEnterprises })(Enterprises)