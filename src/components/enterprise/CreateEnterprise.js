import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createEnterprise, editEnterprise } from '../../store/actions/enterpriseActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup'
import TimesIco from '../../assets/icons/times.svg'

class CreateEnterprise extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Create New Enterprise",

            name: '',
            mobile: '',
            enterpriseEmail: '',
            address: '',
            enterpriseAdminEmail: '',

            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        if (this.props.isEdit.active) {
            this.setState({
                name: this.props.isEdit.payload.Enterprise,
                mobile: this.props.isEdit.payload.Phone,
                enterpriseEmail: this.props.isEdit.payload["Enterprise Email"],
                enterpriseAdminEmail: this.props.isEdit.payload["Enterprise Admin Email"],
                address: this.props.isEdit.payload.Address
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const enterpriseData = {
            name: this.state.name,
            mobile: this.state.mobile,
            enterpriseEmail: this.state.enterpriseEmail,
            enterpriseAdminEmail: this.state.enterpriseAdminEmail,
            address: this.state.address,
        };

        if (this.props.isEdit.active) {
            this.props.editEnterprise(enterpriseData, this.props.history, this.props.isEdit.payload.ignore._id);
        } else {
            this.props.createEnterprise(enterpriseData, this.props.history);
        }

    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const { toggle_modal } = this.props;
        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='name'
                                value={this.state.name}
                                label='Enterprise'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='mobile'
                                value={this.state.mobile}
                                label='Mobile Number'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='enterpriseEmail'
                                value={this.state.enterpriseEmail}
                                label='Enterprise Email'
                                error={null}
                                type='email'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='enterpriseAdminEmail'
                                value={this.state.enterpriseAdminEmail}
                                label='Enterprise Admin Email'
                                error={null}
                                type='email'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='address'
                                value={this.state.address}
                                label='Address'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateEnterprise.propTypes = {
    enterprise: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    enterprise: state.enterprise,
    errors: state.errors
});

export default connect(mapStateToProps, { createEnterprise, editEnterprise })(
    withRouter(CreateEnterprise)
);