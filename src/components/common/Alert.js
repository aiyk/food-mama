import React, { Component } from 'react'
import TimesIco from '../../assets/icons/times.svg'

class Alert extends Component {
    state = {
        openAlerts: false
    };

    render() {
        const { alerts, toggle_modal } = this.props;

        let renderer = null;
        if (alerts) {
            renderer = <div className={"alert-wrap " + (alerts.type ? 'alert-green' : 'alert-red')}>
                <div className='alert-body'>{alerts.msg}</div>
            </div>
        }

        return (<React.Fragment>{renderer}</React.Fragment>)
    }
}

export default Alert