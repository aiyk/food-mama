import React, { Component } from "react";
import Modal from "./Modal";
import CheckboxFieldGroup from "./CheckboxFieldGroup";

import PlusICO_whiteIco from "../../assets/icons/plus-white.svg";
import AngleLeftIco from "../../assets/icons/angle-left.svg";
import AngleRightIco from "../../assets/icons/angle-right.svg";
import FilterIco from "../../assets/icons/filter.svg";
import Redflag from '../../assets/icons/Redflag.svg';
import Moment from 'moment';

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      metaData: this.props.metaData,
      collections: this.props.dataset,

      sortState: true,
      ddmenu_tblmenu: false,
      ddmenu_tblitem: false,
      clickItemIndex: null,
      editTrIndex: null,

      thKeys: null,

      pagination_data: {
        currentPage: 0,
        perPage: 0,
        totalPages: 0
      },

      formData: [],
      error: null,

      payload: {
        name: null,
        val: null
      },
      search: "",
      searchKey: "",

      openPopup: false,
      itemClicked: null,

      itemDetail: null
    };
  }

  metaData = () => {
    return this.state.metaData;
  };
  collections = () => {
    return this.state.collections || [];
  };
  filteredCollections = () => {
    const criteria = this.state.search.toLowerCase();
    const key = this.state.searchKey;
    if (criteria && key) {
      return Object.values(this.state.collections).filter(item => {
        console.log(item);
        if (item[key]) {
          const to_compare = item[key].toLowerCase();
          return to_compare.includes(criteria);
        }
        return this.state.collections;
      });
    } else {
      return this.state.collections;
    }
  };
  sortCollections = sort_key => {
    this.setState({
      sortState: !this.state.sortState
    });
    if (this.state.sortState) {
      this.state.collections.sort(function (a, b) {
        if (a[sort_key] < b[sort_key]) {
          return -1;
        }
        if (a[sort_key] > b[sort_key]) {
          return 1;
        }
        return 0;
      });
    } else {
      this.state.collections.sort(function (a, b) {
        if (a[sort_key] > b[sort_key]) {
          return -1;
        }
        if (a[sort_key] < b[sort_key]) {
          return 1;
        }
        return 0;
      });
    }
  };
  setCollections = payload => {
    this.setState({
      collections: payload
    });
  };

  item_to_show = i => {
    return this.state.clickItemIndex === i;
  };
  item_to_edit = i => {
    return this.state.editTrIndex === i;
  };
  tblmenu_onclick = () => {
    this.setState({
      ddmenu_tblmenu: !this.state.ddmenu_tblmenu
    });
  };
  tblmenuitem_onclick = itemIndex => {
    if (this.state.clickItemIndex === itemIndex) {
      this.setState({
        clickItemIndex: null
      });
    } else {
      this.setState({
        clickItemIndex: itemIndex
      });
    }
    this.setState({
      ddmenu_tblitem: !this.state.ddmenu_tblitem
    });
  };
  action_onclick = (itemIndex, action, id, item) => {
    this.props.callback(action, id, item);
    if (action === "Detail") {
      this.setState({
        openPopup: true,
        itemClicked: itemIndex
      });
      this.tblmenuitem_onclick("");
    }
  };
  editTr_onclick = itemIndex => {
    if (this.state.editTrIndex === itemIndex) {
      this.setState({
        editTrIndex: null
      });
    } else {
      this.setState({
        editTrIndex: itemIndex
      });
    }
    this.tblmenuitem_onclick("");
  };
  deleteTr_onclick = itemIndex => {
    let updatedCollection = [...this.state.collections];
    updatedCollection.splice(itemIndex, 1);
    this.tblmenuitem_onclick("");
  };
  paginate = (collection, page, numItems) => {
    if (!Array.isArray(collection)) {
      throw `Expect array and got ${typeof collection}`;
    }
    const currentPage = parseInt(page);
    const perPage = parseInt(numItems);
    const offset = (page - 1) * perPage;
    const paginatedItems = collection.slice(offset, offset + perPage);

    return {
      currentPage,
      perPage,
      total: collection.length,
      totalPages: Math.ceil(collection.length / perPage),
      data: paginatedItems
    };
  };
  prev = () => {
    let updatedCollection = [...this.state.collections];
    let pagination_data = this.state.pagination_data;
    if (this.state.pagination_data.currentPage <= 1) {
      pagination_data.currentPage = 1;
    } else {
      pagination_data.currentPage -= 1;
    }
    this.setState({
      collection: updatedCollection.collections
    });
  };
  next = () => {
    let updatedCollection = [...this.state.collections];
    let pagination_data = this.state.pagination_data;
    if (
      this.state.pagination_data.currentPage >=
      this.state.pagination_data.totalPages
    ) {
      pagination_data.currentPage = this.state.pagination_data.totalPages;
    } else {
      pagination_data.currentPage += 1;
    }
    this.setState({
      collection: updatedCollection.collections
    });
  };
  loadedCollection = (page = 1, perPage = 5) => {
    // this.search_key = this.searchKey;
    // this.search_val = this.search;

    const getdata = this.filteredCollections();
    let updatePagination = this.state.pagination_data;

    if (!this.state.pagination_data.currentPage) {
      updatePagination.currentPage = page;
      updatePagination.perPage = perPage;
      updatePagination.totalPages = Math.ceil(getdata.length / perPage);
      this.setState({
        pagination_data: updatePagination
      });
    }

    return this.paginate(
      getdata,
      this.state.pagination_data.currentPage,
      this.state.pagination_data.perPage
    );
  };
  collections_keys = () => {
    if (this.state.collections.length > 0) {
      let obj = Object.values(this.state.collections);
      return Object.keys(obj[0]) || [];
    }
    return null;
  };
  handleSearchKey = event => {
    this.setState({
      searchKey: event.target.value
    });
  };
  handleSearchCriteria = event => {
    this.setState({
      search: event.target.value
    });
  };
  onEdit = e => {
    e.preventDefault();
    this.props.handle_create(this.state.formData, this.props.history);
  };
  handleChange = e => {
    const target = this.state.formData[e.target.name];
    this.setState({
      [target]: e.target.value
    });
  };
  toggle_modalDetail = () => {
    this.setState({
      openPopup: !this.state.openPopup
    });
  };

  render() {
    const { toggle_modal } = this.props;
    return (
      <div className="custom-table-wrapper">
        <div id="data-table" className="table-wrap">
          <div className="pre-table">
            {this.state.metaData ? (
              <React.Fragment>
                <div className="table-titles">
                  <div className="table-title">
                    {this.state.metaData.tblTitle}
                  </div>
                  <div className="table-subtitle">
                    {this.state.metaData.tblSubtitle}
                  </div>
                </div>
                <div className="table-ctas">
                  {this.state.metaData.buttons.AddNew.isActive ? (
                    <button onClick={toggle_modal} className="btn btn-blue">
                      <img alt="" src={PlusICO_whiteIco} />{" "}
                      {this.state.metaData.buttons.AddNew.label}
                    </button>
                  ) : null}
                </div>
              </React.Fragment>
            ) : null}
          </div>

          {this.state.collections.length > 0 ? (
            <div>
              <div className="tbl-controls">
                <div className="search-wrap">
                  <select onChange={this.handleSearchKey}>
                    <option value={null}>search key...</option>
                    {this.collections_keys().map(item => {
                      if (item !== "id") {
                        return (
                          <option key={item} value={item}>
                            {item}
                          </option>
                        );
                      }
                    })}
                  </select>
                  <input
                    onChange={this.handleSearchCriteria}
                    type="search"
                    placeholder="search..."
                  />
                </div>

                <div className="tbl-pagination">
                  <span>
                    showing page {this.state.pagination_data.currentPage} out of{" "}
                    {this.state.pagination_data.totalPages} pages
                  </span>
                  <img alt="" onClick={this.prev} src={AngleLeftIco} />
                  <img alt="" onClick={this.next} src={AngleRightIco} />
                </div>
              </div>

              <div className="tbl nice nice-scroll">
                <div>
                  <div className="tr thead">
                    {this.state.metaData.trCheckbox ? (
                      <div className="td-actions" />
                    ) : null}

                    {this.collections_keys().map(th => {
                      if (th === "images") {
                        return <div key={th} className="th" />;
                      }
                      if (th !== "id" && th !== "ignore") {
                        return (
                          <div
                            key={th}
                            onClick={() => this.sortCollections([th])}
                            className="th"
                          >
                            <span>{th}</span>
                            <img alt="" src={FilterIco} />
                          </div>
                        );
                      }
                    })}

                    {this.state.metaData.trActions ? (
                      <div className="td-actions" />
                    ) : null}
                  </div>
                </div>
                {this.loadedCollection().data.map((item, index) => (
                  <div key={index}>
                    {item ? (
                      <div className="tr tbody">
                        {this.state.metaData.trCheckbox ? (
                          <div className="td-actions">
                            <input type="checkbox" />
                            {/* <CheckboxFieldGroup name={item._id} checked={this.state.checkedItems.get(item.name)} onChange={this.handleChange} /> */}
                          </div>
                        ) : null}
                        {Object.values(item).map((val, key) => {
                          if (Object.keys(item)[key] === "images") {
                            return (
                              <div key={key} className="td">
                                <div className="trImg">
                                  <img
                                    alt="food image"
                                    src={val &&
                                      `https://foodmama.ng/uploads/foodimage/` +
                                      val[0]
                                    }
                                  />
                                </div>
                              </div>
                            );
                          } else if (Object.keys(item)[key].toLocaleLowerCase() === "schedule") {
                            return (
                              <div key={key} className="td">
                                <div className="rfImg">
                                  {val != "null" && <img
                                    alt="red flag"
                                    src={Redflag}
                                  />}
                                   <span>{val == "null" ? "Not Scheduled" : Moment(val).format("dddd, MMMM Do YYYY, h:mm a")}</span>
                                </div>
                              </div>
                            );
                          } else if (
                            Object.keys(item)[key] !== "id" &&
                            Object.keys(item)[key] !== "ignore"
                          ) {
                            return (
                              <div key={key} className="td">
                                {this.collections_keys[key] === "Image" ? (
                                  this.item_to_edit(index) ? (
                                    <input
                                      type="file"
                                      className="td-edit-input"
                                    />
                                  ) : (
                                      <div className="trImg">
                                        <img src={val} />
                                      </div>
                                    )
                                ) : typeof val === "object" && val != null ? (
                                  this.item_to_edit(index) ? (
                                    <input
                                      value={Object.values(val)}
                                      onChange={() =>
                                        this.updateCollections(key, item.ID)
                                      }
                                      type="text"
                                      className="td-edit-input"
                                    />
                                  ) : (
                                      <div>
                                        {Object.keys(val).map(
                                          (valValue, valKey) => (
                                            <div className="td-obj" key={valKey}>
                                              {Object.keys(item)[key] ===
                                                "ignore" ? null : (
                                                  <div>
                                                    <span className="valKey">
                                                      {Array.isArray(val) ? null : (
                                                        <span>{valValue}</span>
                                                      )}
                                                    </span>
                                                    <span className="valValue">
                                                      {val[valValue]}
                                                    </span>
                                                  </div>
                                                )}
                                            </div>
                                          )
                                        )}
                                      </div>
                                    )
                                ) : this.item_to_edit(index) ? (
                                  <input
                                    value={val}
                                    onChange={() =>
                                      this.updateCollections(key, item.ID)
                                    }
                                    type="text"
                                    className="td-edit-input"
                                  />
                                ) : (
                                        <span>{val}</span>
                                      )}
                              </div>
                            );
                          }
                        })}
                        {this.state.metaData.trActions ? (
                          <div className="td-actions dropdown-wrap">
                            <button
                              onClick={() => this.tblmenuitem_onclick(index)}
                              className="btn-hollow btn-elipsis-v-center btn-x0"
                            />
                            <div className="dropdown-wrap">
                              {this.item_to_show(index) ? (
                                <ul
                                  onClick={() => this.tblmenuitem_onclick("")}
                                  className="dropdown-menu"
                                >
                                  {item.ignore.actions.map((action, i) => (
                                    <li
                                      key={i}
                                      onClick={() =>
                                        this.action_onclick(
                                          index,
                                          action.label,
                                          item.id,
                                          item
                                        )
                                      }
                                    >
                                      {action.label}
                                    </li>
                                  ))}
                                </ul>
                              ) : null}
                            </div>
                          </div>
                        ) : null}
                      </div>
                    ) : null}
                  </div>
                ))}
              </div>

              <div className="tbl-controls">
                <div className="tbl-pagination">
                  <span>
                    showing page {this.state.pagination_data.currentPage} out of{" "}
                    {this.state.pagination_data.totalPages} pages
                  </span>
                  <img alt="" onClick={this.prev} src={AngleLeftIco} />
                  <img alt="" onClick={this.next} src={AngleRightIco} />
                </div>
                {this.state.metaData.tblSummary ? (
                  <div className="table-subtitle">
                    {this.state.metaData.tblSummary}
                  </div>
                ) : null}
              </div>
              {this.state.openPopup ? (
                <Modal
                  data={this.loadedCollection().data[this.state.itemClicked]}
                  toggle_modal={this.toggle_modalDetail}
                  modalTitle={this.state.metaData.tblTitle}
                />
              ) : null}
            </div>
          ) : (
              <div className="no-data">no data returned</div>
            )}
        </div>
      </div>
    );
  }
}

export default Table;
