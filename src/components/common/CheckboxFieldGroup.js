import React from 'react'
import PropTypes from 'prop-types'

const CheckboxFieldGroup = ({
    name,
    checked,
    label,
    error,
    info,
    onChange,
    disabled
}) => {
    return (
        <div className="check-group">
            <label>{label}</label>
            <input
                type='checkbox'
                name={name}
                className="site-input"
                onChange={onChange}
                disabled={disabled}
                checked={checked}
            />
            {info && <small className="input-info">{info}</small>}
            {error && (<div className="input-err">{error}</div>)}
        </div>
    )
}

CheckboxFieldGroup.propTypes = {
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    info: PropTypes.string,
    error: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.string,
    label: PropTypes.string
}

export default CheckboxFieldGroup;