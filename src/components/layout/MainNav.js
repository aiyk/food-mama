import React from 'react'
import { connect } from 'react-redux'
import { NavLink } from "react-router-dom"
import PropTypes from 'prop-types'
import applogo from '../../assets/logo/foodmamalogo.png'
import './layout.scss';

const MainNav = ({ auth }) => {
    return (
        <div className="nav-outer">
            <div className="main-nav">
                <div className="logo-wrap">
                    <img className="liuImg" src={applogo} alt="" />
                </div>

                <div className="nav-links">
                    {
                        auth.isAuthenticated === true
                            ? (
                                <React.Fragment>
                                    <NavLink to="/dashboard" activeClassName="nav-link-active" className="nav-link">Dashboard</NavLink>
                                    <NavLink to="/orders" activeClassName="nav-link-active" className="nav-link">Orders</NavLink>
                                    {/* <NavLink to="/packages" activeClassName="nav-link-active" className="nav-link">Packages</NavLink> */}
                                    <NavLink to="/dispatch" activeClassName="nav-link-active" className="nav-link">Dispatch</NavLink>
                                    {
                                        localStorage.userRole === 'super-admin'
                                            ? <NavLink to="/users" activeClassName="nav-link-active" className="nav-link">Users</NavLink>
                                            : null
                                    }
                                    {
                                        localStorage.userRole === 'super-admin'
                                            ? <NavLink to="/enterprises" activeClassName="nav-link-active" className="nav-link">Enterprises</NavLink>
                                            : null
                                    }
                                    <NavLink to="/foods" activeClassName="nav-link-active" className="nav-link">Foods</NavLink>
                                    {
                                        localStorage.userRole === 'super-admin'
                                            ? <NavLink to="/branches" activeClassName="nav-link-active" className="nav-link">Branches</NavLink>
                                            : null
                                    }
                                    <NavLink to="/payments" activeClassName="nav-link-active" className="nav-link">Payments</NavLink>
                                    <NavLink to="/remittance" activeClassName="nav-link-active" className="nav-link">Remittances</NavLink>
                                    {
                                        localStorage.userRole === 'super-admin'
                                            ? <NavLink to="/settings" activeClassName="nav-link-active" className="nav-link">System Settings</NavLink>
                                            : null
                                    }
                                </React.Fragment>
                            )
                            : null
                    }
                </div>

                <div></div>
                {/* <div className="nav-footer">food mama copyright &copy; {(new Date()).getFullYear()}</div> */}
            </div>
        </div>
    )
}

MainNav.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(MainNav);