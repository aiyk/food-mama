import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { logoutUser } from '../../store/actions/authActions'
import { clearCurrentUser } from '../../store/actions/userActions'
import Spinner from '../common/Spinner'
import './layout.scss'
import userimg from '../../assets/logo/foodmamalogo.png'

class AppBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: {},
            toggleUserMenu: this.props.usermenu
        }
    }
    toggle_userMenu = ($event) => {
        if ($event.target.className === 'loggedin-user' || $event.target.className === 'liu-img' || $event.target.className === 'liu-name' || $event.target.className === 'liuImg') {
            this.setState({
                toggleUserMenu: !this.state.toggleUserMenu
            })
        }
    }

    render() {
        const onLogoutClick = (e) => {
            e.preventDefault()
            this.props.clearCurrentUser();
            this.props.logoutUser();
        }
        const { isAuthenticated, user } = this.props.auth;

        return (
            <div className="app-bar" >
                <div className="app-bar-lhs app-bar-title">Admin Management Portal</div>
                <div className="custom-table-wrapper">
                    <div className="app-bar-search search-wrap">
                        <input type="search" placeholder="search..." />
                    </div>
                </div>
                {
                    isAuthenticated
                        ? (
                            <div className="app-bar-rhs">
                                <div className="app-bar-icons">
                                    <Link to="/dashboard">
                                        <svg viewBox="0 0 495.398 495.398">
                                            <path d="M487.083,225.514l-75.08-75.08V63.704c0-15.682-12.708-28.391-28.413-28.391c-15.669,0-28.377,12.709-28.377,28.391     v29.941L299.31,37.74c-27.639-27.624-75.694-27.575-103.27,0.05L8.312,225.514c-11.082,11.104-11.082,29.071,0,40.158     c11.087,11.101,29.089,11.101,40.172,0l187.71-187.729c6.115-6.083,16.893-6.083,22.976-0.018l187.742,187.747     c5.567,5.551,12.825,8.312,20.081,8.312c7.271,0,14.541-2.764,20.091-8.312C498.17,254.586,498.17,236.619,487.083,225.514z" fill="#9c9c9c" />
                                            <path d="M257.561,131.836c-5.454-5.451-14.285-5.451-19.723,0L72.712,296.913c-2.607,2.606-4.085,6.164-4.085,9.877v120.401     c0,28.253,22.908,51.16,51.16,51.16h81.754v-126.61h92.299v126.61h81.755c28.251,0,51.159-22.907,51.159-51.159V306.79     c0-3.713-1.465-7.271-4.085-9.877L257.561,131.836z" fill="#9c9c9c" />
                                        </svg>
                                    </Link>
                                    <Link to="/">
                                        <svg viewBox="0 0 268.765 268.765">
                                            <path d="M267.92,119.461c-0.425-3.778-4.83-6.617-8.639-6.617    c-12.315,0-23.243-7.231-27.826-18.414c-4.682-11.454-1.663-24.812,7.515-33.231c2.889-2.641,3.24-7.062,0.817-10.133    c-6.303-8.004-13.467-15.234-21.289-21.5c-3.063-2.458-7.557-2.116-10.213,0.825c-8.01,8.871-22.398,12.168-33.516,7.529    c-11.57-4.867-18.866-16.591-18.152-29.176c0.235-3.953-2.654-7.39-6.595-7.849c-10.038-1.161-20.164-1.197-30.232-0.08    c-3.896,0.43-6.785,3.786-6.654,7.689c0.438,12.461-6.946,23.98-18.401,28.672c-10.985,4.487-25.272,1.218-33.266-7.574    c-2.642-2.896-7.063-3.252-10.141-0.853c-8.054,6.319-15.379,13.555-21.74,21.493c-2.481,3.086-2.116,7.559,0.802,10.214    c9.353,8.47,12.373,21.944,7.514,33.53c-4.639,11.046-16.109,18.165-29.24,18.165c-4.261-0.137-7.296,2.723-7.762,6.597    c-1.182,10.096-1.196,20.383-0.058,30.561c0.422,3.794,4.961,6.608,8.812,6.608c11.702-0.299,22.937,6.946,27.65,18.415    c4.698,11.454,1.678,24.804-7.514,33.23c-2.875,2.641-3.24,7.055-0.817,10.126c6.244,7.953,13.409,15.19,21.259,21.508    c3.079,2.481,7.559,2.131,10.228-0.81c8.04-8.893,22.427-12.184,33.501-7.536c11.599,4.852,18.895,16.575,18.181,29.167    c-0.233,3.955,2.67,7.398,6.595,7.85c5.135,0.599,10.301,0.898,15.481,0.898c4.917,0,9.835-0.27,14.752-0.817    c3.897-0.43,6.784-3.786,6.653-7.696c-0.451-12.454,6.946-23.973,18.386-28.657c11.059-4.517,25.286-1.211,33.281,7.572    c2.657,2.89,7.047,3.239,10.142,0.848c8.039-6.304,15.349-13.534,21.74-21.494c2.48-3.079,2.13-7.559-0.803-10.213    c-9.353-8.47-12.388-21.946-7.529-33.524c4.568-10.899,15.612-18.217,27.491-18.217l1.662,0.043    c3.853,0.313,7.398-2.655,7.865-6.588C269.044,139.917,269.058,129.639,267.92,119.461z M134.595,179.491    c-24.718,0-44.824-20.106-44.824-44.824c0-24.717,20.106-44.824,44.824-44.824c24.717,0,44.823,20.107,44.823,44.824    C179.418,159.385,159.312,179.491,134.595,179.491z" fill="#9c9c9c" />
                                        </svg>
                                    </Link>
                                    <svg onClick={onLogoutClick} viewBox="0 0 438.122 438.122">
                                        <path d="M219.064,0C98.073,0,0.003,98.083,0.003,219.073c0,120.991,98.07,219.049,219.061,219.049s219.055-98.058,219.055-219.049   C438.119,98.083,340.055,0,219.064,0z M199.855,45.805c0-10.613,8.605-19.227,19.23-19.227c10.613,0,19.225,8.614,19.225,19.227   v166.498c0,10.607-8.611,19.219-19.225,19.219c-10.625,0-19.23-8.611-19.23-19.219V45.805z M219.064,399.584   c-95.14,0-172.536-77.391-172.536-172.549c0-81.016,56.223-149,131.658-167.435v33.009   c-57.613,17.549-99.662,71.154-99.662,134.426c0,77.492,63.053,140.531,140.533,140.531c77.498,0,140.524-63.051,140.524-140.531   c0-63.272-42.045-116.877-99.655-134.426V59.601C335.371,78.036,391.6,146.007,391.6,227.035   C391.6,322.193,314.198,399.584,219.064,399.584z" data-original="#000000" data-old_color="#9c9c9c" fill="#9c9c9c" />
                                    </svg>
                                </div>
                                <div onClick={this.toggle_userMenu} className="loggedin-user">
                                    <div className="liu-name">{localStorage.userName}</div>
                                    <div className="liu-img">
                                        <img className="liuImg" src={userimg} alt="" />
                                    </div>
                                </div>

                                {
                                    this.state.toggleUserMenu
                                        ? (
                                            <ul className="user-menus">
                                                <li>Profile</li>
                                                <li>Settings</li>
                                                <li onClick={onLogoutClick}>Logout</li>
                                            </ul>
                                        )
                                        : (null)
                                }

                            </div>
                        )
                        : (
                            <div className="app-bar-rhs">
                                <Link to="/login" className="nav-link">Login</Link>
                            </div>
                        )

                }
            </div>
        )
    }
}

AppBar.propTypes = {
    getCurrentUser: PropTypes.func,
    user: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    user: state.user,
    auth: state.auth
});

export default connect(mapStateToProps, { logoutUser, clearCurrentUser })(AppBar)
