import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from '../common/Spinner'
import { loginUser } from '../../store/actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import './auth.scss'

class Login extends Component {

    state = {
        email: "",
        password: "",
        strategy: 'local',
        errors: {},
        loading: false
    };

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push('/dashboard');
        }
    }
    componentWillReceiveProps = nextProps => {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push('/dashboard');
        }

        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors })
        }
    }
    handleInputChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    handleSubmit = (e) => {
        this.setState({
            loading: true
        })
        e.preventDefault();

        const userData = {
            email: this.state.email,
            password: this.state.password,
            strategy: 'local'
        }

        this.props.loginUser(userData);
    };

    render() {
        const { errors } = this.state;

        return (
            <div className="login-frm-wrap">
                <form onSubmit={this.handleSubmit} >
                    <div className="auth-title">Admin Login</div>
                    <TextFieldGroup
                        type="email"
                        name="email"
                        placeholder="Email"
                        onChange={this.handleInputChange}
                        error={errors.email}
                    />
                    <TextFieldGroup
                        type="password"
                        name="password"
                        placeholder="Password"
                        onChange={this.handleInputChange}
                        error={errors.password}
                    />
                    <button className="btn btn-x2 btn-default">Sign In</button>
                    {this.state.loading
                        ? <div className='full-loader-wrap'><Spinner /></div>
                        : null
                    }

                </form>
            </div>
        );
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);