import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "../common/Spinner";
import {
  getOrders,
  getNewOrders,
  getConfirmedOrders,
  getInTransit,
  getDeliveredOrders,
  getOrderById,
  acceptOrder
} from "../../store/actions/orderActions";
import AllOrders from "./AllOrders";
import ConfirmedOrders from "./ConfirmedOrders";
import InTransit from "./InTransit";
import NewOrders from "./NewOrders";
import DeliveredOrders from "./DeliveredOrders";
import "../users/users.scss";

class Orders extends Component {
  state = {
    openPopup: false,
    activeTab: "1"
  };

  componentDidMount() {
    this.props.getOrders();
    this.props.getConfirmedOrders();
    this.props.getInTransit();
    this.props.getNewOrders();
    this.props.getDeliveredOrders();
  }
  toggle_modal = () => {
    this.setState({
      openPopup: !this.state.openPopup
    });
  };
  tab_click = tab => {
    this.setState({
      activeTab: tab
    });
  };
  render() {
    const {
      metaData,
      collections,
      confirmedOrders,
      inTransitOrders,
      newOrders,
      deliveredOrders,
      orderDetail,
      loading
    } = this.props.orderDetail;

    let renderer;
    if (this.state.activeTab === "1") {
      if (collections === null || loading) {
        renderer = <Spinner />;
      } else {
        renderer = (
          <AllOrders
            orderDetail={orderDetail}
            collections={collections}
            metaData={metaData}
            loading={loading}
            getOrderById={this.props.getOrderById}
            acceptOrder={this.props.acceptOrder}
          />
        );
      }
    }

    if (this.state.activeTab === "2") {
      if (newOrders === null || loading) {
        renderer = <Spinner />;
      } else {
        renderer = (
          <NewOrders
            orderDetail={orderDetail}
            collections={newOrders}
            metaData={metaData}
            loading={loading}
            getOrderById={this.props.getOrderById}
            acceptOrder={this.props.acceptOrder}
          />
        );
      }
    }

    if (this.state.activeTab === "3") {
      if (confirmedOrders === null || loading) {
        renderer = <Spinner />;
      } else {
        renderer = (
          <ConfirmedOrders
            orderDetail={orderDetail}
            collections={confirmedOrders}
            metaData={metaData}
            loading={loading}
            getOrderById={this.props.getOrderById}
          />
        );
      }
    }

    if (this.state.activeTab === "5") {
      if (inTransitOrders === null || loading) {
        renderer = <Spinner />;
      } else {
        renderer = (
          <InTransit
            orderDetail={orderDetail}
            collections={inTransitOrders}
            metaData={metaData}
            loading={loading}
            getOrderById={this.props.getOrderById}
          />
        );
      }
    }

    if (this.state.activeTab === "4") {
      if (deliveredOrders === null || loading) {
        renderer = <Spinner />;
      } else {
        renderer = (
          <DeliveredOrders
            orderDetail={orderDetail}
            collections={deliveredOrders}
            metaData={metaData}
            loading={loading}
            getOrderById={this.props.getOrderById}
          />
        );
      }
    }

    return (
      <div>
        <div className="tab-wrap">
          <ul className="tab-menu">
            <li
              onClick={() => this.tab_click("1")}
              className={
                "tab-menu-item " +
                (this.state.activeTab === "1" ? "active-tab" : null)
              }
            >
              All
            </li>
            <li
              onClick={() => this.tab_click("2")}
              className={
                "tab-menu-item " +
                (this.state.activeTab === "2" ? "active-tab" : null)
              }
            >
              New Orders
            </li>
            <li
              onClick={() => this.tab_click("3")}
              className={
                "tab-menu-item " +
                (this.state.activeTab === "3" ? "active-tab" : null)
              }
            >
              Confirmed Orders
            </li>
            <li
              onClick={() => this.tab_click("5")}
              className={
                "tab-menu-item " +
                (this.state.activeTab === "5" ? "active-tab" : null)
              }
            >
              Orders In Transit
            </li>
            <li
              onClick={() => this.tab_click("4")}
              className={
                "tab-menu-item " +
                (this.state.activeTab === "4" ? "active-tab" : null)
              }
            >
              Delivered Orders
            </li>
          </ul>
          <div className="tab-content">{renderer}</div>
        </div>
      </div>
    );
  }
}

Orders.propTypes = {
  getOrders: PropTypes.func.isRequired,
  getOrderById: PropTypes.func,
  orderDetail: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  orderDetail: state.orders
});

export default connect(
  mapStateToProps,
  {
    getOrders,
    acceptOrder,
    getOrderById,
    getDeliveredOrders,
    getConfirmedOrders,
    getInTransit,
    getNewOrders
  }
)(Orders);
