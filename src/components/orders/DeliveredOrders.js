import React, { Component } from 'react'
import Table from '../common/Table'

export default class DeliveredOrders extends Component {
    actionTrigger = (action, id) => {
        switch (action.toLowerCase()) {
            case 'detail':
                this.props.getOrderById(id);
                break;
            case 'accept order':
                this.props.acceptOrder({
                    action: 'create',
                    orderIds: [id]
                });
                break;
            case 'cancel order':
                break;
            default:
                break;
        }
    }
    render() {
        const {
            metaData,
            collections,
            orderDetail,
            loading
        } = this.props;

        let data_remodel = [];
        let data_remodel_full = [];
        let data_remodel_detail = {
            Customer: null,
            Address: null,
            Food: {
                Food: null,
                Quantity: null,
                Price: null
            },
            Payment: null,
            Total: null,
            Date: null,
            Status: null,
            Schedule: null,
            ignore: {
                id: null,
                isSet: false, // checks if order detail has been populated
                actions: [] // defines table and detail modal actions
            }
        };

        collections.data.forEach(function (entry) {
            data_remodel_detail.Customer = entry.customer.firstName + ' ' + entry.customer.lastName;
            data_remodel_detail.Address = entry.customer.address;
            entry.food.forEach(food => {
                data_remodel_detail.Food = {
                    Food: food.foodName,
                    Price: food.totalCost,
                    Quantity: food.qty
                };
            })
            data_remodel_detail.Payment = entry.paymentStatus;
            data_remodel_detail.Total = entry.totalCost;
            data_remodel_detail.Schedule = entry.schedule;
            data_remodel_detail.Date = entry.createdAt;
            data_remodel_detail.Status = entry.orderStatus;
            data_remodel_detail.ignore.id = entry._id;
            data_remodel_detail.ignore.isSet = true;

            data_remodel_detail.ignore.actions = [];
            if (data_remodel_detail.Status === "Confirmed") {

            } else if (data_remodel_detail.Status === "Created") {

            } else if (data_remodel_detail.Status === "Delivered") {
            } else if (data_remodel_detail.Status === "In-transit") {

            } else {
                if (localStorage.userRole === 'branch-admin') {
                    data_remodel_detail.ignore.actions.push(
                        {
                            label: "Accept Order",
                            colorClass: "btn-outline-blue",
                            icon: "check.svg"
                        }
                    );
                }
            }

            let key_val = {
                Customer: {
                    Customer: null,
                    Address: null
                },
                Food: null,
                Amount: null,
                Schedule: null,
                Status: null,
                Payment: null,
                Created: null,
                ignore: {
                    actions: []
                }
            };

            key_val.Customer.Customer = entry.customer.firstName + ' ' + entry.customer.lastName;
            entry.food.forEach(food => {
                key_val.Food = {
                    Food: food.foodName,
                    Total: food.totalCost
                };
            })
            key_val.Customer.Address = entry.deliveryAddress;
            key_val.id = entry._id;
            key_val.Food = 'Eba and Egusi Soup';
            key_val.Amount = entry.totalCost;
            key_val.Status = entry.orderStatus;
            key_val.Schedule = entry.schedule;
            key_val.Payment = entry.paymentStatus + ' via ' + entry.paymentChannel;
            key_val.Created = entry.createdAt;

            if (entry.orderStatus === 'Confirmed') {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    }
                );
            } else if (entry.orderStatus === 'Created') {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    }
                );
            } else if (entry.orderStatus === 'Delivered') {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    }
                );
            } else {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    }
                );
                if (localStorage.userRole === 'branch-admin') {
                    key_val.ignore.actions.push(
                        {
                            label: "Accept Order",
                            colorClass: "btn-outline-blue",
                            icon: "check.svg"
                        }
                    );
                }
            }

            data_remodel.push(key_val);
        });

        return (
            <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={data_remodel_detail}
                loading={loading}
                callback={this.actionTrigger}
                toggle_modal={this.toggle_modal}
            />
        )
    }
}
