import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createBranch, editBranch, getBranchManagers } from '../../store/actions/branchActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Select from 'react-select'
import TextFieldGroup from '../common/TextFieldGroup'
import TimesIco from '../../assets/icons/times.svg'

class CreateBranch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Create New Branch",

            branch: '',
            address: '',
            branchManager: '',

            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        this.props.getBranchManagers();
        if (this.props.isEdit.active) {
            this.setState({
                branch: this.props.isEdit.payload.Branch,
                address: this.props.isEdit.payload.Address
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const branchData = {
            name: this.state.branch,
            address: this.state.address,
            adminProfileId: this.state.branchManager
        };

        if (this.props.isEdit.active) {
            this.props.editBranch(branchData, this.props.history, this.props.isEdit.payload.ignore._id);
        } else {
            this.props.createBranch(branchData, this.props.history);
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleManager = (e) => {
        this.setState({ branchManager: e.value });
    }
    render() {
        const { toggle_modal, loading } = this.props;

        // Select options for manager
        const managerOptions = [];
        let renderManager;

        if (this.props.branchManagers === null || loading || this.props.branchManagers === undefined) {
            renderManager = <Select
                name="branchManager"
                isLoading={true}
                placeholder="Select Branch Manager"
                onChange={this.handleManager}
                options={managerOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />
        } else {
            this.props.branchManagers.data.forEach(val => {
                managerOptions.push(
                    { label: val.firstName + ' ' + val.lastName, value: val._id }
                )
            })

            renderManager = <Select
                name="branchManager"
                isLoading={false}
                placeholder="Select Branch Manager"
                onChange={this.handleManager}
                options={managerOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />

        }

        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='branch'
                                value={this.state.branch}
                                label='Branch Name'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='address'
                                value={this.state.address}
                                label='Address'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />

                            {renderManager}

                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateBranch.propTypes = {
    branch: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    branch: state.branch,
    branchManagers: state.branch.branchManagers,
    errors: state.errors
});

export default connect(mapStateToProps, { createBranch, editBranch, getBranchManagers })(
    withRouter(CreateBranch)
);