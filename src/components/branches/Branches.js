import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getBranches } from '../../store/actions/branchActions'
import Table from '../common/Table'
import CreateBranch from './CreateBranch'
import '../users/users.scss'

class Branches extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getBranches();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        }, () => !this.state.openPopup && this.nullEditPayload())
    }
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "deactivate":
                break;
            default:
                break;
        }
    };
    nullEditPayload = () => this.setState({isEdit : {
        active: false,
        payload: null
    }})
    render() {
        const {
            metaData,
            collections,
            loading
        } = this.props.branch;

        let data_remodel = [];
        let data_remodel_full = [];
        let data_remodel_detail = {
            Branch: null,
            Address: null,
            Manager: {
                Name: null,
                Phone: null,
                Email: null
            },
            Date: null,
            id: null,
            ignore: {
                actions: [],
                _id: null,
                isSet: false, // checks if order detail has been populated
            }
        }
        let renderer;
        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.data.forEach(function (entry) {
                let key_val = {
                    Branch: null,
                    Address: null,
                    Manager: {
                        Name: null,
                        Phone: null,
                        Email: null
                    },
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null
                    }
                };

                key_val.Branch = entry.name;
                key_val.Address = entry.address;
                if (entry.hasOwnProperty('manager')) {
                    key_val.Manager.Name = entry.manager.firstName + ' ' + entry.manager.lastName;
                    key_val.Manager.Phone = entry.manager.mobileNo;
                    key_val.Manager.Email = entry.manager.email;
                }
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;

                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    },
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Delete",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );
                data_remodel.push(key_val);

                data_remodel_detail.Branch = entry.name;
                data_remodel_detail.Address = entry.address;
                if (entry.hasOwnProperty('manager')) {
                    data_remodel_detail.Manager.Name = entry.manager.firstName + ' ' + entry.manager.lastName;
                    data_remodel_detail.Manager.Phone = entry.manager.mobileNo;
                    data_remodel_detail.Manager.Email = entry.manager.email;
                }
                data_remodel_detail.Created = entry.createdAt;
                data_remodel_detail.id = entry._id;
                data_remodel_detail.ignore._id = entry._id;

                data_remodel_detail.ignore.actions = [];
                data_remodel_detail.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Delete",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );

                data_remodel_detail.ignore.isSet = true;
            });

            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                dataDetail={data_remodel_detail}
                metaData={metaData}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createDispatch}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateBranch isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div >
        )
    }
}

Branches.propTypes = {
    getBranches: PropTypes.func.isRequired,
    branch: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    branch: state.branch
});

export default connect(mapStateToProps, { getBranches })(Branches)