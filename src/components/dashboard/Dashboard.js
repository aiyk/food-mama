import React, { Component } from 'react'
import DbStats from './DbStats'
import DbStats2 from './DbStats2'
import DbNewOrders from './sections/DbNewOrders'
import DbFoods from './sections/DbFoods'
import DbAvailableRiders from './sections/DbAvailableRiders'
import DbAssignedPackages from './sections/DbAssignedPackages'
import DbBranches from './sections/DbBranches'
import DbPackages from './sections/DbPackages'
import DbUsers from './sections/DbUsers'
import './dashboard.scss'
var LineChart = require("react-chartjs").Line;
var BarChart = require("react-chartjs").Bar;

class Dashboard extends Component {
    render() {
        let data = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,0.8)",
                    highlightFill: "rgba(151,187,205,0.75)",
                    highlightStroke: "rgba(151,187,205,1)",
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]
        };

        return (
            <div className="dashboard-wrap">
                <DbStats />
                <div className="db-section-wrap">
                    <div className="db-section db-sect-50 nice nice-scroll">
                        <LineChart data={data} width="500" height="250" />
                    </div>
                    <div className="db-section db-sect-50 nice nice-scroll">
                        <DbNewOrders />
                    </div>
                </div>

                <div className="db-section-wrap db-has-border">
                    {/* <div className="db-section db-sect-50 nice nice-scroll">
                        <DbAssignedPackages />
                    </div>
                    <div className="db-section db-sect-50 nice nice-scroll">
                        <DbUsers />
                    </div> */}
                    <DbStats2 />
                </div>
                {/* 
                <div className="db-section-wrap">
                    <div className="db-section db-sect-50 nice nice-scroll">
                        <DbFoods />
                    </div>
                    <div className="db-section db-sect-50 nice nice-scroll">
                        <BarChart data={data} width="500" height="250" />
                    </div>
                </div>

                <div className="db-section-wrap db-has-borders">
                    <div className="db-section db-sect-32 nice nice-scroll">
                        <DbAvailableRiders />
                    </div>
                    <div className="db-section db-sect-32 nice nice-scroll">
                        <DbBranches />
                    </div>
                    <div className="db-section db-sect-32 nice nice-scroll">
                        <DbUsers />
                    </div>
                </div> */}

            </div>
        )
    }
}

export default Dashboard