import React, { Component } from 'react'
import { Link } from "react-router-dom"
import './dashboard.scss'
import DbStat from './DbStat';
import { connect } from 'react-redux';

class DbStats extends Component {
    render() {
        const { analytics } = this.props;

        const riders = {
            color: 'green',
            head: `${analytics["Rider"] || 0}`,
            label: 'Riders',
            img: 'pie'
        }
        const branches = {
            color: 'orange',
            head: `${analytics["Branches"] || 0}`,
            label: 'Branches',
            img: 'pie'
        }
        const enterprises = {
            color: 'blue',
            head: `${analytics["Enterprises"] || 0}`,
            label: 'Enterprises',
            img: 'people'
        }
        const menu = {
            color: 'red',
            head: `${analytics["Meals"] || 0}`,
            label: 'Food On Menu',
            img: 'bike'
        }
        return (
            <div className="db-stats">
                <Link to="/riders"><DbStat data={riders} /></Link>
                <Link to="/branches"><DbStat data={branches} /></Link>
                <Link to="/enterprises"><DbStat data={enterprises} /></Link>
                <Link to="/foods"><DbStat data={menu} /></Link>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    analytics: state.config.analytics
})

export default connect(mapStateToProps, {})(DbStats)