import React, { Component } from 'react'
import { Link } from "react-router-dom"
import './dashboard.scss'
import DbStat from './DbStat';
import { connect } from 'react-redux';
import { getAnalytics } from '../../store/actions/settingsActions';

class DbStats extends Component {

    componentDidMount() {
        this.props.getAnalytics("super-admin");
    }

    render() {
        const { analytics } = this.props;
        const totalSales = {
            color: 'blue',
            head: `\u20A6 ${analytics["Gross Sales"] || 0}`,
            label: "Total Sales",
            img: 'pie'
        }
        const productsSold = {
            color: 'red',
            head: `${analytics["Total Sales"] || 0}`,
            label: 'Products Sold',
            img: 'pie'
        }
        const newCustomers = {
            color: 'green',
            head: `${analytics["Total Customers"] || 0}`,
            label: 'Total Customers',
            img: 'people'
        }
        const delivered = {
            color: 'orange',
            head: `${analytics["Total Sales"] || 0}`,
            label: 'Successful Deliveries',
            img: 'bike'
        }
        return (
            <div className="db-stats">
                <Link to="/orders"><DbStat data={productsSold} /></Link>
                <Link to="/orders"><DbStat data={totalSales} /></Link>
                <Link to="/orders"><DbStat data={newCustomers} /></Link>
                <Link to="/dispatch"><DbStat data={delivered} /></Link>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    analytics: state.config.analytics
})

export default connect(mapStateToProps, { getAnalytics })(DbStats);