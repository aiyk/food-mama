import React, { Component } from 'react'
import './dashboard.scss'

class DbStat extends Component {
    render() {
        const { color, head, label, img } = this.props.data;

        return (
            <div className={"db-stat " + 'stat-' + color}>
                <div className="stat-top">
                    <div>
                        <div className="stat-head">{head}</div>
                        <div className="stat-label">{label}</div>
                    </div>
                    <div></div>
                </div>
                <div className="stat-bottom-liner"></div>
            </div>
        )
    }
}

export default DbStat