import React, { Component } from 'react'
import { connect } from 'react-redux'
import Spinner from '../../common/Spinner'
import { Link } from "react-router-dom"
import { getUsers } from '../../../store/actions/userActions'

class DbUsers extends Component {
    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const {
            collections,
            loading
        } = this.props.user;
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            renderer = <div className="list-item-body nice nice-scroll">
                {this.props.user.collections.map((item, index) =>
                    <div key={item._id}>
                        <div className="item-row">
                            <div className="list-item1">
                                {item.firstName + ' ' + item.lastName}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        }

        return (
            <div>
                <div className="db-sect-header">
                    <div className="db-sect-title">Users</div>
                    <div className="db-header-actions">
                        <Link to="/users" className="db-header-action">GoTo Page</Link>
                    </div>
                </div>
                <div className="db-sect-body">
                    {renderer}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

export default connect(mapStateToProps,
    { getUsers }
)(DbUsers)