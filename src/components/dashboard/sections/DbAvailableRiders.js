import React, { Component } from 'react'
import { connect } from 'react-redux'
import Spinner from '../../common/Spinner'
import { Link } from "react-router-dom"
import { getAvailableRiders } from '../../../store/actions/packageActions'

class DbAvailableRiders extends Component {
    componentDidMount() {
        this.props.getAvailableRiders();
    }

    render() {
        const {
            availableRiders,
            loading
        } = this.props;
        let renderer;

        if (this.props.availableRiders === null || loading || this.props.availableRiders === undefined) {
            renderer = <Spinner />
        } else {
            renderer = <div className="list-item-body nice nice-scroll">
                {this.props.availableRiders.data.map((rider, index) =>
                    <div key={rider._id}>
                        <div className="item-row">
                            <div className="list-item">
                                {rider.firstName} {rider.lastName}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        }

        return (
            <div>
                <div className="db-sect-header">
                    <div className="db-sect-title">Available Riders</div>
                    <div className="db-header-actions">
                        {/* <Link to="/foods" className="db-header-action">GoTo Page</Link> */}
                    </div>
                </div>
                <div className="db-sect-body">
                    {renderer}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    availableRiders: state.packages.availableRiders
});

export default connect(mapStateToProps,
    { getAvailableRiders }
)(DbAvailableRiders)