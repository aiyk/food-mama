import React, { Component } from 'react'
import { connect } from 'react-redux'
import Spinner from '../../common/Spinner'
import { Link } from "react-router-dom"
import { getNewOrders } from '../../../store/actions/orderActions'

class DbNewOrders extends Component {
    componentDidMount() {
        this.props.getNewOrders();
    }

    render() {
        const { newOrders, loading } = this.props.orders;
        let renderer;

        if (newOrders === null || loading) {
            renderer = <Spinner />
        } else {
            renderer = <div className="list-item-body nice nice-scroll">
                {newOrders.data.map((item, index) =>
                    <div key={item._id}>
                        <div className="item-row">
                            <div className="list-item1">
                                {item.customer.firstName + ' ' + item.customer.lastName}
                            </div>
                            <div className="list-item">
                                &#x20A6;{item.totalCost}
                            </div>
                            <div className="list-item">
                                {item.paymentStatus}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        }

        return (
            <div>
                <div className="db-sect-header">
                    <div className="db-sect-title">New Orders</div>
                    <div className="db-header-actions">
                        <Link to="/orders" className="db-header-action">GoTo Page</Link>
                    </div>
                </div>
                <div className="db-sect-body">
                    <div className="list-item-head item-row">
                        <div className="list-item1">Customer</div>
                        <div className="list-item">Amount</div>
                        <div className="list-item">Status</div>
                    </div>
                    {renderer}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    orders: state.orders
});

export default connect(mapStateToProps,
    { getNewOrders }
)(DbNewOrders)