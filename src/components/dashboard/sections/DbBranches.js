import React, { Component } from 'react'
import { connect } from 'react-redux'
import Spinner from '../../common/Spinner'
import { Link } from "react-router-dom"
import { getBranches } from '../../../store/actions/branchActions'

class DbBranches extends Component {
    componentDidMount() {
        this.props.getBranches();
    }
    render() {
        const {
            collections,
            loading
        } = this.props.branch;
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            renderer = <div className="list-item-body nice nice-scroll">
                {this.props.branch.collections.data.map((item, index) =>
                    <div key={item._id}>
                        <div className="item-row">
                            <div className="list-item1">
                                {item.name}
                            </div>
                            <div className="list-item">
                                {item.category}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        }

        return (
            <div>
                <div className="db-sect-header">
                    <div className="db-sect-title">Branch Stores</div>
                    <div className="db-header-actions">
                        <Link to="/branches" className="db-header-action">GoTo Page</Link>
                    </div>
                </div>
                <div className="db-sect-body">
                    <div className="list-item-head item-row">
                        <div className="list-item1">Branch</div>
                        <div className="list-item">Phone</div>
                    </div>
                    {renderer}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    branch: state.branch
});

export default connect(mapStateToProps,
    { getBranches }
)(DbBranches)