import React, { Component } from 'react'
import { connect } from 'react-redux'
import Spinner from '../../common/Spinner'
import { Link } from "react-router-dom"
import { getAssignedPackages } from '../../../store/actions/packageActions'

class DbAssignedPackages extends Component {
    componentDidMount() {
        this.props.getAssignedPackages();
    }

    render() {
        const {
            assignedPackages,
            loading
        } = this.props;
        let renderer;

        if (this.props.assignedPackages === null || loading || this.props.assignedPackages === undefined) {
            renderer = <Spinner />
        } else {
            renderer = <div className="list-item-body nice nice-scroll">
                {this.props.assignedPackages.data.map((item, index) =>
                    <div key={item._id}>
                        <div className="item-row">
                            <div className="list-item1">
                                {item.orderId}
                            </div>
                            <div className="list-item">
                                {item.riderId}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        }

        return (
            <div>
                <div className="db-sect-header">
                    <div className="db-sect-title">Assigned Packages</div>
                    <div className="db-header-actions">
                        <Link to="/foods" className="db-header-action">GoTo Page</Link>
                    </div>
                </div>
                <div className="db-sect-body">
                    <div className="list-item-head item-row">
                        <div className="list-item1">Customer</div>
                        <div className="list-item">Order</div>
                    </div>
                    {renderer}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    assignedPackages: state.packages.assignedPackages
});

export default connect(mapStateToProps,
    { getAssignedPackages }
)(DbAssignedPackages)