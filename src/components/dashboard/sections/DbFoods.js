import React, { Component } from 'react'
import { connect } from 'react-redux'
import Spinner from '../../common/Spinner'
import { Link } from "react-router-dom"
import { getFoods } from '../../../store/actions/foodActions'

class DbFoods extends Component {
    componentDidMount() {
        this.props.getFoods();
    }

    render() {
        const {
            collections,
            loading
        } = this.props.food;
        let renderer;

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            renderer = <div className="list-item-body nice nice-scroll">
                {collections.data.map((item, index) =>
                    <div key={item._id}>
                        <div className="item-row">
                            <div className="list-item1">
                                {item.name}
                            </div>
                            <div className="list-item">
                                {item.category}
                            </div>
                            <div className="list-item">
                                &#x20A6;{item.price}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        }

        return (
            <div>
                <div className="db-sect-header">
                    <div className="db-sect-title">Food Menu</div>
                    <div className="db-header-actions">
                        <Link to="/foods" className="db-header-action">GoTo Page</Link>
                    </div>
                </div>
                <div className="db-sect-body">
                    <div className="list-item-head item-row">
                        <div className="list-item1">Food</div>
                        <div className="list-item">Category</div>
                        <div className="list-item">Price</div>
                    </div>
                    {renderer}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    food: state.food
});

export default connect(mapStateToProps,
    { getFoods }
)(DbFoods)