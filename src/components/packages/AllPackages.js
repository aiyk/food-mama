import React, { Component } from 'react'
import Table from '../common/Table'

export default class AllPackages extends Component {

    actionTrigger = (action, id) => {
        switch (action.toLowerCase()) {
            case 'detail':
                this.props.getPackageById(id);
                break;
            case 'cancel package':
                break;
            default:
                break;
        }
    }
    render() {
        const {
            metaData,
            collections,
            packageDetail,
            loading
        } = this.props;

        let data_remodel = [];
        let data_remodel_full = [];
        let data_remodel_detail = {

            Customer: {
                Customer: null,
                Address: null
            },
            Order: [],
            Status: null,
            Amount: null,
            Payment: null,
            Channel: null,
            Assigned: null,
            Confirmed: null,
            Delivered: null,
            Rider: null,
            Date: null,

            ignore: {
                isSet: false, // checks if detail has been populated
                riderId: null,
                packageId: null,
                orderId: null,
                customerId: null,
                actions: [] // defines table and detail modal actions
            }
        };

        //checks if a package detail data set is available and populates the detail modal
        if (packageDetail !== null) {
            Object.entries(packageDetail).forEach(([key, value]) => {
                if (key === 'order') {
                    data_remodel_detail.Customer.Customer = value.customer.firstName + ' ' + value.customer.lastName;
                    data_remodel_detail.Customer.Address = value.customer.address;
                    data_remodel_detail.Status = value.orderStatus;
                    data_remodel_detail.Amount = value.totalCost;
                    data_remodel_detail.Payment = value.paymentStatus;
                    data_remodel_detail.ignore.customerId = value.customer._id;
                }

                if (key === 'detail') {
                    let orderItem = {
                        Food: null,
                        Category: null,
                        Quantity: null,
                        Price: null
                    }
                    value.forEach(function (entry) {
                        orderItem.Food = entry.food.name
                        orderItem.Category = entry.food.category
                        orderItem.Quantity = entry.qty
                        orderItem.Price = entry.unitPrice
                    });

                    data_remodel_detail.Order.push(orderItem);
                }

                if (key === 'pack') {
                    data_remodel_detail.Channel = value.paymentChannel;
                    data_remodel_detail.Rider = value.riderId;
                    data_remodel_detail.Date = value.createdAt;

                    data_remodel_detail.ignore.riderId = value.riderId;
                    data_remodel_detail.ignore.packageId = value._id;
                    data_remodel_detail.ignore.orderId = value.orderId;

                    if (value.isAssigned) {
                        data_remodel_detail.Assigned = 'YES';
                    } else {
                        data_remodel_detail.Assigned = '-';
                    }

                    if (value.isConfirmed) {
                        data_remodel_detail.Confirmed = 'YES';
                    } else {
                        data_remodel_detail.Confirmed = '-';
                    }

                    if (value.isDelivered) {
                        data_remodel_detail.Delivered = 'YES';
                    } else {
                        data_remodel_detail.Delivered = '-';
                    }

                }

            });
            data_remodel_detail.ignore.isSet = true;

            if (data_remodel_detail.Status === 'Confirmed' || data_remodel_detail.Status === 'Ordered') {
                data_remodel_detail.ignore.actions.push(
                    {
                        label: "Cancel Order",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );
            } else if (data_remodel_detail.Status === 'Created') {
                data_remodel_detail.ignore.actions.push(
                    {
                        label: "Cancel Order",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );
            } else {
                data_remodel_detail.ignore.actions.push(
                    {
                        label: "Accept Order",
                        colorClass: "btn-outline-blue",
                        icon: "check.svg"
                    },
                    {
                        label: "Cancel Order",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );
            }
        }

        collections.data.forEach(function (entry) {
            let key_val = {
                Date: null,
                //Order: null,
                Assigned: null,
                Confirmed: null,
                Delivered: null,
                Payment: null,
                //Rider: null,
                id: null,
                ignore: {
                    actions: [],
                    _id: null
                }
            };

            key_val.Date = entry.createdAt;
            // key_val.Order = entry.orderId;
            if (entry.isAssigned) {
                key_val.Assigned = 'YES';
            } else {
                key_val.Assigned = '-';
            }
            if (entry.isConfirmed) {
                key_val.Confirmed = 'YES';
            } else {
                key_val.Confirmed = '-';
            }
            if (entry.isDelivered) {
                key_val.Delivered = 'YES';
            } else {
                key_val.Delivered = '-';
            }

            key_val.Payment = entry.paymentChannel;
            // key_val.Rider = entry.riderId;
            key_val.id = entry._id;
            key_val.ignore._id = entry._id;

            if (entry.isAssigned) {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    },
                    {
                        label: "Cancel Order",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );
            } else if (entry.isConfirmed) {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    },
                    {
                        label: "Cancel Order",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );
            } else if (entry.isDelivered) {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    },
                    {
                        label: "Cancel Order",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );
            } else {
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    },
                    {
                        label: "Accept Order",
                        colorClass: "btn-outline-blue",
                        icon: "check.svg"
                    },
                    {
                        label: "Cancel Order",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );
            }

            data_remodel.push(key_val);
        });

        return (
            <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={data_remodel_detail}
                loading={loading}
                callback={this.actionTrigger}
                toggle_modal={this.toggle_modal}
            />
        )
    }
}
