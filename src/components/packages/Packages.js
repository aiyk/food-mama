import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import {
    getPackages,
    getAssignedPackages,
    getUnassignedPackages,
    getPackageById
} from '../../store/actions/packageActions'
import AllPackages from './AllPackages';
import AssignedPackages from './AssignedPackages';
import UnassignedPackages from './UnassignedPackages';
import '../users/users.scss'

class Packages extends Component {

    state = {
        openPopup: false,
        activeTab: '1'
    }

    componentDidMount() {
        this.props.getPackages();
        this.props.getAssignedPackages();
        this.props.getUnassignedPackages();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        })
    }
    tab_click = (tab) => {
        this.setState({
            activeTab: tab
        })
    }
    render() {
        const {
            metaData,
            collections,
            assignedPackages,
            unassignedPackages,
            packageDetail,
            loading
        } = this.props.packageDetail;

        let renderer;
        if (this.state.activeTab === '1') {
            if (collections === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <AllPackages
                    packageDetail={packageDetail}
                    collections={collections}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                />
            }
        }

        if (this.state.activeTab === '2') {
            if (assignedPackages === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <AssignedPackages
                    packageDetail={packageDetail}
                    collections={assignedPackages}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                />
            }
        }

        if (this.state.activeTab === '3') {
            if (unassignedPackages === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <UnassignedPackages
                    packageDetail={packageDetail}
                    collections={unassignedPackages}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                />
            }
        }

        return (
            <div>
                <div className="tab-wrap">
                    <ul className="tab-menu">
                        <li onClick={() => this.tab_click('1')} className={"tab-menu-item " + (this.state.activeTab === '1' ? 'active-tab' : null)}>All</li>
                        <li onClick={() => this.tab_click('2')} className={"tab-menu-item " + (this.state.activeTab === '2' ? 'active-tab' : null)}>Assigned Packages</li>
                        <li onClick={() => this.tab_click('3')} className={"tab-menu-item " + (this.state.activeTab === '3' ? 'active-tab' : null)}>Unassigned Packages</li>
                    </ul>
                    <div className="tab-content">
                        {renderer}
                    </div>
                </div>
            </div>
        )
    }
}

Packages.propTypes = {
    getPackages: PropTypes.func.isRequired,
    getPackageById: PropTypes.func,
    packageDetail: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    packageDetail: state.packages
});

export default connect(mapStateToProps,
    {
        getPackages,
        getPackageById,
        getAssignedPackages,
        getUnassignedPackages
    }
)(Packages)