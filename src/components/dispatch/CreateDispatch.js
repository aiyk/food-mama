import React, { Component } from 'react'
import { connect } from 'react-redux';
import { getAvailableRiders } from '../../store/actions/packageActions'
import { getUnassignedPackages } from '../../store/actions/packageActions'
import { createDispatch } from '../../store/actions/dispatchActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Select from 'react-select'
import TimesIco from '../../assets/icons/times.svg'

class CreateDispatch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Assign Order To Rider",

            packages: [],
            rider: '',

            errors: {}
        }
    }
    componentDidMount() {
        this.props.getAvailableRiders();
        this.props.getUnassignedPackages();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const dispatchData = {
            "action": "assign",
            "trackingNumbers": this.state.packages,
            "riderId": this.state.rider
        };

        this.props.createDispatch(dispatchData, this.props.history);
    }
    handlePackages = (e) => {
        this.setState({ packages: [...this.state.packages, e[0].value] });
    }
    handleRider = (e) => {
        this.setState({ rider: e.value });
    }
    render() {
        const { toggle_modal, loading } = this.props;

        // Select options for rider
        const riderOptions = [];
        const packageOptions = [];

        let renderRider;
        let renderPackages;
        let renderer;

        if (this.props.availableRiders === null || loading || this.props.availableRiders === undefined) {
            renderRider = <Select
                name="rider"
                isLoading={true}
                placeholder="Select Rider"
                onChange={this.handleRider}
                options={riderOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />
        } else {
            this.props.availableRiders.data.forEach(val => {
                riderOptions.push(
                    { label: val.firstName + ' ' + val.lastName, value: val._id }
                )
            })

            renderRider = <Select
                name="rider"
                isLoading={false}
                placeholder="Select Rider"
                onChange={this.handleRider}
                options={riderOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />
        }

        if (this.props.packages === null || loading || this.props.packages === undefined) {
            renderPackages = <Select
                name="packages"
                isMulti
                isLoading={true}
                placeholder="Select Packages"
                onChange={this.handlePackages}
                options={packageOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />
        } else {
            //console.log(this.props.packages);
            this.props.packages.data.forEach(val => {
                //console.log(val);
                packageOptions.push(
                    {
                        label: val.order.customer.firstName
                            + ' ' + val.order.customer.lastName
                            + ' - (' + val.trackingNumber + ')',
                        value: val.trackingNumber
                    }
                )
            })

            renderPackages = <Select
                name="packages"
                isMulti
                isLoading={false}
                placeholder="Select Packages"
                onChange={this.handlePackages}
                options={packageOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />

        }

        renderer = <form onSubmit={this.onSubmit}>
            {renderPackages}
            {renderRider}
            <button className="btn btn-blue">Submit</button>
        </form>

        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>

                    <div className="modal-content">
                        {renderer}
                    </div>
                </div>
            </div>
        )
    }
}

CreateDispatch.propTypes = {
    dispatch: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    dispatch: state.dispatch,
    availableRiders: state.packages.availableRiders,
    packages: state.packages.unassignedPackages,
    errors: state.errors
});

export default connect(mapStateToProps, { createDispatch, getUnassignedPackages, getAvailableRiders })(
    withRouter(CreateDispatch)
);