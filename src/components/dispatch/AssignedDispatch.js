import React, { Component } from 'react'
import Table from '../common/Table'

export default class AssignedDispatch extends Component {

    actionTrigger = (action, id) => {
        switch (action.toLowerCase()) {
            case 'detail':
                this.props.getPackageById(id);
                break;
            case 'cancel package':
                break;
            default:
                break;
        }
    }
    render() {
        const {
            metaData,
            collections,
            packageDetail,
            loading
        } = this.props;

        let data_remodel = [];
        let data_remodel_full = [];
        let data_remodel_detail = {

            Customer: {
                Name: null,
                Address: null
            },
            Rider: {
                Name: null,
                Phone: null
            },
            Order: {
                Food: [],
                Amount: null,
                Tracker: null
            },
            Status: null,
            Date: null,
            id: null,
            ignore: {
                _id: null,
                isSet: false, // checks if order detail has been populated
                actions: []
            }
        };

        collections.data.forEach(function (entry) {
            let key_val = {
                Customer: {
                    Name: null,
                    Address: null
                },
                Rider: {
                    Name: null,
                    Phone: null
                },
                Order: {
                    Food: [],
                    Amount: null,
                    Tracker: null
                },
                Status: null,
                Date: null,
                id: null,
                ignore: {
                    _id: null,
                    actions: []
                }
            };

            key_val.Customer.Name = entry.order.customer.firstName + ' ' + entry.order.customer.lastName;
            key_val.Customer.Address = entry.order.deliveryAddress;
            key_val.Rider.Name = entry.rider.firstName + ' ' + entry.rider.lastName;
            key_val.Rider.Phone = entry.rider.mobileNo;
            entry.order.food.forEach(food => {
                key_val.Order.Food.push(food.foodName);
            })
            key_val.Order.Amount = '#' + entry.order.totalCost;
            key_val.Order.Tracker = entry.trackingNumber;
            key_val.Status = entry.order.orderStatus;
            key_val.Date = entry.createdAt;
            key_val.id = entry._id;
            key_val.ignore._id = entry._id;

            key_val.ignore.actions.push(
                {
                    label: "Detail",
                    colorClass: "btn-outline-grey",
                    icon: "elipsis-h.svg"
                }
            );

            data_remodel_detail.Customer.Name = entry.order.customer.firstName + ' ' + entry.order.customer.lastName;
            data_remodel_detail.Customer.Address = entry.order.customer.address;
            data_remodel_detail.Rider.Name = entry.rider.firstName + ' ' + entry.rider.lastName;
            data_remodel_detail.Rider.Phone = entry.rider.mobileNo;
            entry.order.food.forEach(food => {
                data_remodel_detail.Order.Food.push(food.foodName);
            })
            data_remodel_detail.Order.Amount = '#' + entry.order.totalCost;
            data_remodel_detail.Order.Tracker = entry.trackingNumber;
            data_remodel_detail.Status = entry.order.orderStatus;
            data_remodel_detail.Date = entry.createdAt;
            data_remodel_detail.id = entry._id;
            data_remodel_detail.ignore._id = entry._id;
            data_remodel_detail.ignore.isSet = true;

            data_remodel_detail.ignore.actions = []
            // data_remodel_detail.ignore.actions.push(
            //     {
            //         label: "Detail",
            //         colorClass: "btn-outline-grey",
            //         icon: "elipsis-h.svg"
            //     }
            // );

            data_remodel.push(key_val);
        });

        return (
            <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={data_remodel_detail}
                loading={loading}
                callback={this.actionTrigger}
                toggle_modal={this.props.toggle_modal}
            />
        )
    }
}
