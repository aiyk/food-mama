import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import {
    getPackages,
    getAssignedPackages,
    getUnassignedPackages,
    getPackageById
} from '../../store/actions/packageActions'
import AllDispatch from './AllDispatch';
import AssignedDispatch from './AssignedDispatch';
import UnassignedDispatch from './UnassignedDispatch';
import { createDispatch } from '../../store/actions/dispatchActions'
import Table from '../common/Table'
import CreateDispatch from './CreateDispatch'
import '../users/users.scss'

class Dispatchs extends Component {
    state = {
        openPopup: false,
        activeTab: '1'
    }

    componentDidMount() {
        this.props.getPackages();
        this.props.getAssignedPackages();
        this.props.getUnassignedPackages();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        })
    }
    tab_click = (tab) => {
        this.setState({
            activeTab: tab
        })
    }
    render() {
        const {
            metaData,
            collections,
            assignedPackages,
            unassignedPackages,
            packageDetail,
            loading
        } = this.props.packageDetail;

        let renderer;
        if (this.state.activeTab === '1') {
            if (collections === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <AllDispatch
                    packageDetail={packageDetail}
                    collections={collections}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                    toggle_modal={this.toggle_modal}
                />
            }
        }

        if (this.state.activeTab === '2') {
            if (assignedPackages === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <AssignedDispatch
                    packageDetail={packageDetail}
                    collections={assignedPackages}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                    toggle_modal={this.toggle_modal}
                />
            }
        }

        if (this.state.activeTab === '3') {
            if (unassignedPackages === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <UnassignedDispatch
                    packageDetail={packageDetail}
                    collections={unassignedPackages}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                    toggle_modal={this.toggle_modal}
                />
            }
        }

        return (
            <div>
                <div className="tab-wrap">
                    <ul className="tab-menu">
                        <li onClick={() => this.tab_click('1')} className={"tab-menu-item " + (this.state.activeTab === '1' ? 'active-tab' : null)}>All</li>
                        <li onClick={() => this.tab_click('2')} className={"tab-menu-item " + (this.state.activeTab === '2' ? 'active-tab' : null)}>Assigned Dispatches</li>
                        <li onClick={() => this.tab_click('3')} className={"tab-menu-item " + (this.state.activeTab === '3' ? 'active-tab' : null)}>Unassigned Dispatches</li>
                    </ul>
                    <div className="tab-content">
                        {renderer}
                        {
                            this.state.openPopup
                                ? (<CreateDispatch toggle_modal={this.toggle_modal} />)
                                : (null)
                        }
                    </div>
                </div>
            </div>
        )
    }
}

Dispatchs.propTypes = {
    getPackages: PropTypes.func.isRequired,
    getPackageById: PropTypes.func,
    packageDetail: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    packageDetail: state.packages
});

export default connect(mapStateToProps,
    {
        getPackages,
        getPackageById,
        getAssignedPackages,
        getUnassignedPackages
    }
)(Dispatchs)