import React, { Component } from "react";
import { connect } from "react-redux";
import {
  createFood,
  editFood,
  getFoodCategories,
  addFoodToBranch
} from "../../store/actions/foodActions";
import { systemAlert } from '../../store/actions/settingsActions';
import { getFoods } from '../../store/actions/foodActions'
import Table from '../common/Table'
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import ImageUploader from "react-images-upload";
import TextFieldGroup from "../common/TextFieldGroup";
import CheckboxFieldGroup from "../common/CheckboxFieldGroup";
import SelectListGroup from "../common/SelectListGroup";
import TimesIco from "../../assets/icons/times.svg";
import Select from 'react-select';
import axios from 'axios';

class CreateFood extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalTitle: "Create New Food",

      food: "",
      category: "",
      preOrder: false,
      price: "",
      description: "",
      img: null,
      pictures: [],
      mealTypeIds: [],
      foodTypeId: "",
      mealTypeDefaults: null,
      errors: {},
      foodId: "",
      images: []
    };
    this.onDrop = this.onDrop.bind(this);
  }

  componentDidMount() {
    this.props.getFoodCategories();
    if (this.props.isEdit.active) {
      let mealTypes = this.props.isEdit.payload.ignore.mealTypeIds ? this.props.meal.data
        .filter(meal => this.props.isEdit.payload.ignore.mealTypeIds.includes(meal._id))
        .map(meal => ({ label: meal.name, value: meal._id })) : [];
      this.setState({
        food: this.props.isEdit.payload.Food,
        category: this.props.isEdit.payload.Category,
        preOrder: this.props.isEdit.payload.ignore.preOrder,
        price: this.props.isEdit.payload.Price,
        description: this.props.isEdit.payload.ignore.desc,
        foodTypeId: this.props.isEdit.payload.ignore.foodTypeId,
        mealTypeIds: this.props.isEdit.payload.ignore.mealTypeIds || [],
        mealTypeDefaults: mealTypes,
        foodId: this.props.isEdit.payload.id,
        images: this.props.isEdit.payload.images || []
      });
    } else {
      this.setState({ mealTypeDefaults: [] })
    }
  }

  onDrop(picture) {
    this.setState({
      pictures: this.state.pictures.concat(picture)
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit = e => {
    e.preventDefault();

    let foodData = new FormData();

    Object.keys(this.state).map(ky => {
      if (ky == "pictures") {
        if (this.state[ky].length > 0) {
          this.state[ky].map(image => foodData.append("images", image));
        }
      } else if (ky == "food") {
        foodData.append("name", this.state[ky]);
      } else if (ky == "mealTypeIds") {
        this.state[ky].map(mealTypeIds => foodData.append("mealTypeIds", mealTypeIds));
      } else if (ky != "mealTypeIds" || ky != "food" || ky != "pictures" || ky != "modalTitle" || ky != "errors", ky != "mealTypeDefaults") {
        foodData.append(ky, this.state[ky]);
      }
    })

    // foodData.append("name", this.state.food);
    // foodData.append("category", this.state.category);
    // foodData.append("preOrder", this.state.preOrder);
    // foodData.append("price", this.state.price);
    // foodData.append("description", this.state.description);
    // foodData.append("foodTypeId", this.state.foodTypeId);
    // this.state.pictures.map(picture => foodData.append("images", picture));
    // this.state.mealTypeIds.map(mealIds => foodData.append("mealTypeIds", mealIds));

    if (this.props.isEdit.active) {
      this.props.editFood(
        foodData,
        this.props.isEdit.payload.ignore._id
      );
    } else {
      this.props.createFood(foodData);
    }
    this.props.toggle_modal()
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleCheck = () => {
    this.setState({ preOrder: !this.state.preOrder });
  };
  actionTrigger = (action, id, item) => {
    switch (action.toLowerCase()) {
      case "add to branch list":
        this.props.addFoodToBranch({
          foodIds: [item.ignore._id]
        });
        this.props.toggle_modal();
        break;
      default:
        break;
    }
  };
  handleMeals = (e, a) => {
    const { removedValue } = a;
    if (removedValue) {
      let mealTypeIds = this.state.mealTypeIds.filter(id => id != removedValue.value);
      return this.setState({ mealTypeIds });
    }
    this.setState({ mealTypeIds: [...this.state.mealTypeIds, e[e.length - 1].value] });
  }

  _deleteImage = async (imageName) => {
    let deleteImage = await axios.delete(`/manage-images/${imageName}?type=food`);
    if (deleteImage) {
      let images = this.state.images.filter(img => img != imageName);
      this.setState({ images }, () => {
        this.props.getFoods();
        this.props.systemAlert("Image deleted successfully");
      });
    }
  }

  render() {
    const { toggle_modal, data_remodel, data_remodel_full, loading, metadata } = this.props;
    // Select options for food category
    const options = [{ label: "Select a food type...", value: null }];
    let mealTypes = this.props.meal ? this.props.meal.data.map(meal => ({
      label: meal.name,
      value: meal._id
    })) : [];

    let foodTypes = this.props.foodTypes.collections ? this.props.foodTypes.collections.data.map(types => ({
      label: types.foodType,
      value: types._id
    })) : [];

    foodTypes.unshift(options[0]);

    if (this.props.food.foodCategories) {
      this.props.food.foodCategories.data.forEach(function (value) {
        options.push({ label: value.name, value: value.name });
      });
    }

    return (
      <div className="modal-overlay">
        <div className="modal-wrap shadow">
          {
            localStorage.userRole === 'super-admin'
              ? (
                <React.Fragment>
                  {metadata.trCheckbox = false}
                  <div className="modal-header">
                    <div className="modal-title">{this.state.modalTitle}</div>
                    <img alt="" onClick={toggle_modal} src={TimesIco} />
                  </div>

                  <div className="modal-content">
                    <form onSubmit={this.onSubmit}>
                      <div>
                        {this.state.images.map((image, index) =>
                          <div>
                            <img alt="" onClick={() => this._deleteImage(image)} src={TimesIco} />
                            <img key={index * 232} src={`https://foodmama.ng/uploads/foodimage/${image}`} style={{ height: "100px", width: "100px" }} />
                          </div>
                        )}
                      </div>
                      <TextFieldGroup
                        name="food"
                        value={this.state.food}
                        label="Food"
                        error={null}
                        type="text"
                        onChange={this.handleChange}
                      />
                      {this.state.mealTypeDefaults && <Select
                        name="mealTypeIds"
                        isMulti
                        isLoading={true}
                        defaultValue={this.state.mealTypeDefaults}
                        placeholder="Select meals to show in"
                        onChange={this.handleMeals}
                        options={mealTypes}
                        className="basic-multi-select"
                        classNamePrefix="select"
                      />}
                      <SelectListGroup
                        name="foodTypeId"
                        value={this.state.foodTypeId}
                        onChange={this.handleChange}
                        options={foodTypes}
                        error={null}
                        label="Food Type"
                      />

                      <CheckboxFieldGroup
                        name="preOrder"
                        checked={this.state.preOrder}
                        label="Pre Order"
                        onChange={this.handleCheck}
                      />
                      <TextFieldGroup
                        name="price"
                        value={String(this.state.price)}
                        label="Price"
                        error={null}
                        type="number"
                        onChange={this.handleChange}
                      />
                      <TextFieldGroup
                        name="description"
                        value={this.state.description}
                        label="Description"
                        error={null}
                        type="text"
                        onChange={this.handleChange}
                      />
                      <ImageUploader
                        withIcon={true}
                        buttonText="Upload Food Images"
                        onChange={this.onDrop}
                        withPreview={true}
                        imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                        maxFileSize={5242880}
                      />
                      <button className="btn btn-blue">Submit</button>
                    </form>
                  </div>
                </React.Fragment>
              )
              : (null)
          }
          {
            localStorage.userRole === 'branch-admin'
              ? (
                <div className="modal-padding">
                  <div className="hide">
                    {metadata.buttons.AddNew.isActive = false}
                    {metadata.trActions = true}
                    {metadata.tblSummary = ''}
                    {metadata.tblTitle = 'Global Food List'}
                    {metadata.tblSubtitle = 'Select to add to branch inventory'}
                  </div>
                  <img className="close-absolute" alt="" onClick={toggle_modal} src={TimesIco} />

                  <Table
                    dataset={data_remodel}
                    dataset_full={data_remodel_full}
                    metaData={metadata}
                    dataDetail={null}
                    loading={loading}
                    callback={this.actionTrigger}
                    handle_create={this.props.createFood}
                    toggle_modal={this.toggle_modal}
                  />
                </div>
              )
              : null
          }
        </div>
      </div>
    );
  }
}

CreateFood.propTypes = {
  food: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  food: state.food,
  meal: state.meal.collections,
  foodTypes: state.foodTypes,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { createFood, addFoodToBranch, editFood, getFoodCategories, systemAlert, getFoods }
)(withRouter(CreateFood));
