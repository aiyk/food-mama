import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getFoods, getBranchFoods, removeFoodFromBranch, createFood, deleteFood } from '../../store/actions/foodActions'
import Table from '../common/Table'
import CreateFood from './CreateFood'
import '../users/users.scss'
import { getMeals, getFoodTypes } from '../../store/actions/settingsActions';

class Foods extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        }
    }

    componentDidMount() {
        this.props.getFoods();
        this.props.getMeals();
        this.props.getFoodTypes();
        if (localStorage.userRole === 'branch-admin') {
            this.props.getBranchFoods();
        }
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        }, () => !this.state.openPopup && this.nullEditPayload())
    }

    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "remove from branch list":
                this.props.removeFoodFromBranch(item.ignore._id);
                break;
            case "delete":
                this.props.deleteFood(id);
            case "deactivate":
                break;
            default:
                break;
        }
    };


    nullEditPayload = () => this.setState({isEdit : {
        active: false,
        payload: null
    }})

    render() {
        const {
            metaData,
            branchFoods,
            collections,
            loading
        } = this.props.food;

        let dataset = null;

        //create a new object for popup to avoid object reference problems
        let metaData_clone = JSON.parse(JSON.stringify(metaData));

        //metadata remodel to check uer role
        if (localStorage.userRole === 'super-admin') {
            dataset = collections;
            metaData.buttons.AddNew.label = 'Create Food';
        }
        if (localStorage.userRole === 'branch-admin') {
            dataset = branchFoods;
            metaData.buttons.AddNew.label = 'Add From Food Global List';
        }

        let data_remodel = [];
        let data_remodel_full = [];
        let data_remodel_minimal = [];
        let renderer;

        if (dataset === null || loading) {
            renderer = <Spinner />
        } else {
            //for minimal list used at popup list
            collections.data.forEach(function (entry) {
                let key_val_min = {
                    images: [],
                    Food: null,
                    Price: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null,
                        foodTypeId: "",
                        mealTypeIds: []
                    }
                }

                key_val_min.images = entry.images;
                key_val_min.Food = entry.name;
                key_val_min.Price = entry.price;
                key_val_min.id = entry._id;
                key_val_min.ignore._id = entry._id;
                key_val_min.ignore.foodTypeId = entry.foodTypeId;
                key_val_min.ignore.mealTypeIds = entry.mealTypeIds;
                key_val_min.ignore.actions.push(
                    {
                        label: "Add To Branch List",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    }
                );

                data_remodel_minimal.push(key_val_min);
            })

            //for main list
            dataset.data.forEach(function (entry) {
                let key_val = {
                    images: [],
                    Food: null,
                    Category: null,
                    ["Pre Order"]: null,
                    Price: null,
                    //Description: null,
                    Date: null,
                    id: null,
                    ignore: {
                        actions: [],
                        _id: null,
                        img: null,
                        desc: null,
                        foodTypeId: "",
                        mealTypeIds: [],
                        preOrder: false
                    }
                };

                key_val.images = entry.images;
                key_val.Food = entry.name;
                key_val.Category = entry.category;
                if (entry.preOrder) {
                    key_val['Pre Order'] = 'YES';
                } else {
                    key_val['Pre Order'] = '-';
                }
                key_val.Price = entry.price;
                //key_val.Description = entry.description;
                key_val.ignore.desc = entry.description;
                key_val.Date = entry.createdAt;
                key_val.id = entry._id;
                key_val.ignore._id = entry._id;
                key_val.ignore.img = entry.imageurl;
                key_val.ignore.preOrder = entry.preOrder;
                key_val.ignore.foodTypeId = entry.foodTypeId;
                key_val.ignore.mealTypeIds = entry.mealTypeIds;

                if (localStorage.userRole === 'branch-admin') {
                    key_val.ignore.actions.push(
                        {
                            label: "Remove From Branch List",
                            colorClass: "btn-outline-red",
                            icon: "trash.svg"
                        }
                    );
                }
                key_val.ignore.actions.push(
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "edit.svg"
                    },
                    {
                        label: "Delete",
                        colorClass: "btn-outline-red",
                        icon: "trash.svg"
                    }
                );

                data_remodel.push(key_val);
            });
            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                metaData={metaData}
                dataDetail={null}
                loading={loading}
                callback={this.actionTrigger}
                handle_create={this.props.createFood}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateFood
                            isEdit={this.state.isEdit}
                            toggle_modal={this.toggle_modal}
                            data_remodel={data_remodel_minimal}
                            loading={loading}
                            metadata={metaData_clone}
                        />)
                        : (null)
                }
            </div>
        )
    }
}

Foods.propTypes = {
    getFoods: PropTypes.func.isRequired,
    getBranchFoods: PropTypes.func.isRequired,
    food: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    food: state.food
});

export default connect(mapStateToProps, { getFoods, getBranchFoods, removeFoodFromBranch, createFood, deleteFood, getMeals, getFoodTypes })(Foods)