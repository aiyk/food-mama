import React, { Component } from 'react'
import Spinner from '../common/Spinner'
import Table from '../common/Table'
import CreateUser from './CreateUser'
import './users.scss'

class Staff extends Component {

    state = {
        openPopup: false,
        isEdit: {
            active: false,
            payload: null
        },
        dataset: []
    }
    actionTrigger = (action, id, item) => {
        switch (action.toLowerCase()) {
            case "detail":
                //this.props.getOrderById(id);
                break;
            case "edit":
                this.state.isEdit.active = true;
                this.state.isEdit.payload = item;
                this.toggle_modal();
                break;
            case "deactivate":
                break;
            default:
                break;
        }
    };
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        })
    }
    render() {
        const { metaData, collections, actions, loading } = this.props;
        let renderer;
        let data_remodel = [];
        let data_remodel_full = [];
        let data_remodel_detail = {
            Name: null,
            Phone: null,
            Email: null,
            Address: null,
            Plans: null,
            Role: [],
            Menu: null,
            Wallet: null,
            Created: null,
            ignore: {
                actions: [],
                _id: null,
                isSet: false, // checks if order detail has been populated
            }
        }

        if (collections === null || loading) {
            renderer = <Spinner />
        } else {
            collections.forEach(function (entry) {
                let key_val = {
                    Name: null,
                    Phone: null,
                    Email: null,
                    Role: null,
                    Address: null,
                    ignore: {
                        actions: [],
                        id: null
                    }
                };

                let key_val_others = {
                    ID: null,
                    Created: null,
                    Firstname: null,
                    Lastname: null,
                    Plans: null,
                    Role: null,
                    Menu: null,
                    Wallet: null,
                    Updated: null
                }

                key_val.Name = entry.firstName + ' ' + entry.lastName;
                key_val.Phone = entry.mobileNo;
                key_val.Email = entry.email;
                key_val.Role = entry.role;
                key_val.Address = entry.address;
                key_val.ignore.id = entry._id;
                key_val.ignore.actions.push(
                    {
                        label: "Detail",
                        colorClass: "btn-outline-grey",
                        icon: "elipsis-h.svg"
                    },
                    {
                        label: "Edit",
                        colorClass: "btn-outline-blue",
                        icon: "check.svg"
                    },
                    {
                        label: "Deactivate",
                        colorClass: "btn-outline-red",
                        icon: "times.svg"
                    }
                );

                data_remodel_detail.Name = entry.firstName + ' ' + entry.lastName;
                data_remodel_detail.Phone = entry.mobileNo;
                data_remodel_detail.Email = entry.email;
                data_remodel_detail.Address = entry.address;
                data_remodel_detail.Created = entry.createdAt;
                data_remodel_detail.Plan = entry.plan;
                data_remodel_detail.Role = entry.role;
                data_remodel_detail.Menu = entry.menu;
                data_remodel_detail.Wallet = entry.wallet;
                data_remodel_detail.ignore.isSet = true;

                key_val_others.ID = entry._id;
                key_val_others.Created = entry.createdAt;
                key_val_others.Firstname = entry.firstName;
                key_val_others.Lastname = entry.lastName;
                key_val_others.Plan = entry.plan;
                key_val_others.Role = entry.role;
                key_val_others.Menu = entry.menu;
                key_val_others.Wallet = entry.wallet;
                key_val_others.Updated = entry.updatedAt;

                data_remodel.push(key_val);
                data_remodel_full.push(key_val);
                data_remodel_full.push(key_val_others);
            });

            data_remodel_detail.ignore.actions.push(
                {
                    label: "Edit",
                    colorClass: "btn-outline-blue",
                    icon: "check.svg"
                },
                {
                    label: "Deactivate",
                    colorClass: "btn-outline-red",
                    icon: "times.svg"
                }
            );
            renderer = <Table
                dataset={data_remodel}
                dataset_full={data_remodel_full}
                dataDetail={data_remodel_detail}
                metaData={metaData}
                actions={actions}
                loading={loading}
                callback={this.actionTrigger}
                toggle_modal={this.toggle_modal}
            />
        }

        return (
            <div>
                {renderer}
                {
                    this.state.openPopup
                        ? (<CreateUser isEdit={this.state.isEdit} toggle_modal={this.toggle_modal} />)
                        : (null)
                }
            </div>
        )
    }
}
export default Staff