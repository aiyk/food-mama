import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner'
import { getUsers } from '../../store/actions/userActions'
import Customers from './Customers';
import Staff from './Staff';
import CreateUser from './CreateUser'
import './users.scss'

class Users extends Component {

    state = {
        openPopup: false,
        activeTab: '1'
    }

    componentDidMount() {
        this.props.getUsers();
    }
    toggle_modal = () => {
        this.setState({
            openPopup: !this.state.openPopup
        })
    }
    tab_click = (tab) => {
        this.setState({
            activeTab: tab
        })
    }
    render() {
        const { metaData, actions, collections, loading } = this.props.user;
        let renderer;
        let customersList = [];
        let staffList = [];

        if (collections) {
            collections.map(val => {
                if (val.hasOwnProperty('role')) {
                    staffList.push(val);
                } else {
                    customersList.push(val);
                }
            })
        }
        if (this.state.activeTab === '1') {
            if (collections === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <Customers
                    packageDetail={null}
                    collections={customersList}
                    actions={actions}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                />
            }
        }

        if (this.state.activeTab === '2') {
            if (collections === null || loading) {
                renderer = <Spinner />
            } else {
                renderer = <Staff
                    packageDetail={null}
                    collections={staffList}
                    actions={actions}
                    metaData={metaData}
                    loading={loading}
                    getPackageById={this.props.getPackageById}
                />
            }
        }

        return (
            <div>
                <div className="tab-wrap">
                    <ul className="tab-menu">
                        <li onClick={() => this.tab_click('1')} className={"tab-menu-item " + (this.state.activeTab === '1' ? 'active-tab' : null)}>Customers</li>
                        <li onClick={() => this.tab_click('2')} className={"tab-menu-item " + (this.state.activeTab === '2' ? 'active-tab' : null)}>Staff</li>
                    </ul>
                    <div className="tab-content">
                        {renderer}
                    </div>
                </div>
            </div>
        )
    }
}

Users.propTypes = {
    getUsers: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    user: state.user
});

export default connect(mapStateToProps, { getUsers })(Users)