import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createUser, editUser, getUserRoles } from '../../store/actions/userActions';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup'
import Select from 'react-select'
import TimesIco from '../../assets/icons/times.svg'

class CreateUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalTitle: "Create New User",

            firstname: '',
            lastname: '',
            mobileNo: '',
            email: '',
            address: '',
            roleId: '',
            roleName: '',

            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    componentDidMount() {
        this.props.getUserRoles();
        if (this.props.isEdit.active) {
            //this.forceUpdate();
            const names = this.props.isEdit.payload.Name.split(' ');
            this.setState({
                firstname: names[0],
                lastname: names[1],
                mobileNo: this.props.isEdit.payload.Phone,
                email: this.props.isEdit.payload.Email,
                roleName: this.props.isEdit.payload.Role,
                address: this.props.isEdit.payload.Address
            })
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const userData = {
            firstName: this.state.firstname,
            lastName: this.state.lastname,
            mobileNo: this.state.mobileNo,
            email: this.state.email,
            address: this.state.address,
            roleId: this.state.roleId,
            roleName: this.state.roleName,
            password: '12345678'
        };
        console.log(userData);
        if (this.props.isEdit.active) {
            this.props.editUser(userData, this.props.history, this.props.isEdit.payload.ignore.id);
        } else {
            this.props.createUser(userData, this.props.history);
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleRole = (e) => {
        console.log(e);
        this.setState({
            roleId: e[0].value,
            roleName: e[0].label
        });
    }
    render() {
        const { toggle_modal } = this.props;

        // Select options for roles
        const rolesOptions = [];
        let renderRole;

        if (this.props.user.userRoles === null || this.props.user.userRoles === undefined) {
            renderRole = <Select
                name="role"
                isLoading={true}
                isMulti
                placeholder="Select user role"
                onChange={this.handleRole}
                options={rolesOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />
        } else {
            this.props.user.userRoles.forEach(val => {
                rolesOptions.push(
                    { label: val.name, value: val._id }
                )
            })

            renderRole = <Select
                name="role"
                isLoading={false}
                isMulti
                placeholder="Select user role"
                onChange={this.handleRole}
                options={rolesOptions}
                className="basic-multi-select"
                classNamePrefix="select"
            />

        }

        return (
            <div className="modal-overlay">
                <div className="modal-wrap shadow">
                    <div className="modal-header">
                        <div className="modal-title">{this.state.modalTitle}</div>
                        <img alt="" onClick={toggle_modal} src={TimesIco} />
                    </div>
                    <div className="modal-content">
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                name='firstname'
                                value={this.state.firstname}
                                label='First Name'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='lastname'
                                value={this.state.lastname}
                                label='Last Name'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='mobileNo'
                                value={this.state.mobileNo}
                                label='Mobile Number'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <TextFieldGroup
                                name='email'
                                value={this.state.email}
                                label='Email Address'
                                error={null}
                                type='email'
                                onChange={this.handleChange}
                            />
                            {renderRole}
                            <TextFieldGroup
                                name='address'
                                value={this.state.address}
                                label='Address'
                                error={null}
                                type='text'
                                onChange={this.handleChange}
                            />
                            <button className="btn btn-blue">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

CreateUser.propTypes = {
    user: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    user: state.user,
    errors: state.errors
});

export default connect(mapStateToProps, { createUser, editUser, getUserRoles })(
    withRouter(CreateUser)
);